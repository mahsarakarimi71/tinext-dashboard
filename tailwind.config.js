/** @type {import('tailwindcss').Config} */

module.exports = {
  content: ['./src/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        primary: {
          100: '#EEF8ED',
          400: '#56B94A',
          500: '#339527',
          700: '#29771E',
          800: '#339527'
        },
        tRed: '#EC3F3F',
        tGray: '#F7F7F7',
        masterBlack: '#3A3A3A',
        white: '#FFF',
        gray: {
          100: '#D8D8D8',
          200: '#989898',
          300: '#E2E2E2'
        },
        tOrange: {
          100: '#F8F2ED',
          400: '#F27D1B'
        }
      },
      fontFamily: {
        yekan: ['Yekan', 'sans'],
        irSans: ['IRANSans', 'sans']
      },
      keyframes: {
        'fade-in': {
          '0%': {
            opacity: 0
          },
          '100%': {
            opacity: 1
          }
        }
      },
      animation: {
        'fade-in': 'fade-in 1s ease-in-out'
      }
    }
  },
  plugins: []
}

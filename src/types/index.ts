import { ReactNode } from 'react'

export type GeneralProps = {
  children?: ReactNode
}

export type SidebarData = {
  id: number
  text: string
  link: string
  icon: string
}

export type ApiErr = {
  message: string
}

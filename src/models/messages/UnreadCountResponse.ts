export type UnreadCountResponse = {
  unread_messages_count: number
}

export interface PaginatedResponse<T> {
  data: T[]
  meta: {
    pagination: Pagination
  }
}

export type Pagination = {
  count: number
  current_page: number
  total_pages: number
  total: number
}

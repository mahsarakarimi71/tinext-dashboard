export type Corporate = {
  id: number
  address: string
  description: string
  created_at: string
  email: string
  name: string
  phone_no: string
}

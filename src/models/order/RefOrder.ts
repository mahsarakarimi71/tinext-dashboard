import { Corporate } from './Corporate'
import { Depot } from './Depot'

export type RefOrder = {
  id: number
  barcode_status: string
  cash_on_delivery: string
  code: string
  corporate: Corporate
  fa_backoffie_status: string
  fa_corporate_status: string
  fa_service_type: string
  pickup_at: string
  pickup_timespan: string
  pickup_type: string
  service_type: string
  status: string
  status_text: string
  created_at: string
  is_paid: number
  cost: number | null
  depot: Depot
}

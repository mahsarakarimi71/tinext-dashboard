import { Coordinate } from '../Coordinate'

export type Depot = {
  address: string
  agent_name: string
  agent_phone_no: string
  id: number
  label: string
  plate_no: string
  unit_no: string
  location: Coordinate
  default_depot: number
}

export type Order = {
  id: number
  cash_on_delivery: number
  code: string
  created_at: string
  description?: string
  expected_deliver_timespan: string
  fa_postage_type: string
  fa_status: string
  receiver_address: string
  receiver_fullname: string
  receiver_phone_no: string
  ref_order_code: string
  status: string
  postage: number
}

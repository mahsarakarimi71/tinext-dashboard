export type Ticket = {
  category_name: string
  code: string
  corporate_name: string
  fa_priority: string
  fa_status: string
  id: number
  user_full_name: string
  title: string
  status: 'pending'
  message: string
}

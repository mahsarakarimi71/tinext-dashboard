export type User = {
  created_at: string
  email: string
  full_name: string
  id: number
  mobile: string | null
  balance?: number
  cash_on_delivery_balance?: number
  first_name: string | null
  last_name: string | null
}

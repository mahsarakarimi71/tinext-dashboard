import * as React from 'react'
import {
  Button,
  CenterLoading,
  ColumnType,
  Heading,
  IndexIndicator,
  MobileTable,
  Pagination,
  Table
} from '@/components'
import { useGetTicketList } from './hooks'
import { Ticket } from '@/models'
import { TicketStatus } from './components'
import { useNavigate } from 'react-router-dom'

const TicketsList = () => {
  const [page, setPage] = React.useState(1)
  const { isLoading, data } = useGetTicketList(page)

  const navigate = useNavigate()

  const onPageChanged = (pageNumber: number) => {
    setPage(pageNumber)
  }

  const openTicket = () => {
    navigate('/support/open-ticket')
  }

  const columns: ColumnType[] = [
    {
      label: 'موضوع',
      accessor: 'title',
      sortable: false
    },
    {
      label: 'توضیح',
      accessor: 'message',
      sortable: false
    },
    {
      label: 'شماره تیکت',
      accessor: 'code',
      sortable: false
    },
    {
      label: 'پیوست',
      accessor: 'download_custom',
      sortable: false,
      Component: () => {
        return <button>Download</button>
      }
    },
    {
      label: 'وضعیت',
      accessor: 'status',
      sortable: true,
      Component: ({ rowData }: { rowData: Ticket }) => {
        return (
          <TicketStatus
            status={rowData.status}
            statusText={rowData.fa_status}
          />
        )
      }
    }
  ]

  if (isLoading) {
    return <CenterLoading />
  }

  return (
    <div>
      <div className="mb-14 flex items-center justify-between">
        <Heading title="پشتیبانی" />
        <div className="flex items-center">
          <h3 className="ml-4 hidden text-lg font-bold md:block">
            مرکز تماس: ۰۲۱-۹۹۸۸۷۷۶۶
          </h3>
          <Button onClick={openTicket} type="outlined">
            <Button.Text>ثبت تیکت جدید</Button.Text>
          </Button>
        </div>
      </div>
      <Table data={data?.data || []} columns={columns} page={page} />
      <div className="lg:hidden">
        {data?.data?.map((item, index) => (
          <MobileViewItem
            item={item}
            index={(page - 1) * 15 + index + 1}
            key={index}
          />
        ))}
      </div>
      <Pagination
        currentPage={page}
        totalCount={data?.meta.pagination.total || 0}
        totalPages={data?.meta.pagination.total_pages || 0}
        onPageChanged={onPageChanged}
      />
    </div>
  )
}

const MobileViewItem = ({ item, index }: { item: Ticket; index: number }) => {
  return (
    <MobileTable>
      <MobileTable.Row justify="between">
        <div className="flex items-center">
          <IndexIndicator index={index} withMargin={false} />
          <p className="mr-2 text-sm font-bold text-masterBlack">
            {item.title}
          </p>
        </div>
      </MobileTable.Row>
      <MobileTable.Row justify="between">
        <p className="text-sm text-masterBlack">{item.message}</p>
      </MobileTable.Row>
      <MobileTable.Row justify="between">
        <TicketStatus status={item.status} statusText={item.fa_status} />
      </MobileTable.Row>
    </MobileTable>
  )
}

export default TicketsList

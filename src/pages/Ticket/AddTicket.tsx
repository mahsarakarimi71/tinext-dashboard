import { Formik } from 'formik'
import * as Yup from 'yup'
import { Button, ErrorCaption, Heading, Input } from '@/components'
import { useCreateTicket } from './hooks'
import { displayToast } from '@/lib'

const SCHEMA = Yup.object().shape({
  title: Yup.string().required('لطفا موضوع را وارد کنید'),
  message: Yup.string().required('لطفا توضیحات را وارد کنید')
})

const AddTicket = () => {
  const { mutate, isLoading } = useCreateTicket()

  return (
    <div>
      <Heading title="ثبت تیکت جدید" />
      <Formik
        validationSchema={SCHEMA}
        initialValues={{ title: '', message: '' }}
        onSubmit={(values, { resetForm }) => {
          mutate(
            {
              ...values,
              priority: 1,
              category: 0
            },
            {
              onSuccess: (d) => {
                displayToast(d.message, 'success')
                resetForm()
              },
              onError: (e) => displayToast(e.message, 'error')
            }
          )
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit
        }) => (
          <form onSubmit={handleSubmit}>
            <div className="relative mx-auto mt-10 w-full lg:w-3/4">
              <Input.Label text="موضوع" />
              <Input.Editor
                name="title"
                value={values.title}
                onChange={handleChange}
                onBlur={handleBlur}
                model={errors.title && touched.title ? 'error' : 'normal'}
              />
              {errors.title && touched.title && (
                <ErrorCaption>{errors.title}</ErrorCaption>
              )}
            </div>
            <div className="relative mx-auto my-14 w-full lg:w-3/4">
              <Input.Label text="توضیحات" />
              <Input.Editor
                name="message"
                value={values.message}
                onChange={handleChange}
                onBlur={handleBlur}
                model={errors.message && touched.message ? 'error' : 'normal'}
              />
              {errors.message && touched.message && (
                <ErrorCaption>{errors.message}</ErrorCaption>
              )}
            </div>
            <div className="mx-auto w-full text-left lg:w-3/4">
              <Button btnType="submit" disabled={isLoading}>
                {isLoading ? (
                  <Button.Loading />
                ) : (
                  <Button.Text>ثبت تیکت جدید</Button.Text>
                )}
              </Button>
            </div>
          </form>
        )}
      </Formik>
    </div>
  )
}

export default AddTicket

import { Api } from '@/lib'
import { PaginatedResponse, Ticket } from '@/models'

export const getTicketsList = (
  page = 1
): Promise<PaginatedResponse<Ticket>> => {
  return Api.req('GET', `/api/corporate/v1/ticket?page=${page}`)
}

export const createTicket = (data: { title: string; message: string }) => {
  return Api.req('POST', `/api/corporate/v1/ticket`, data)
}

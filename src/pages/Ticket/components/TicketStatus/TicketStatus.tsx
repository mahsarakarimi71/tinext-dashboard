type Props = {
  status: 'pending'
  statusText: string
}

const getBgColor = (status: 'pending') => {
  switch (status) {
    case 'pending':
      return 'bg-tOrange-100'
  }
}

const getTextColor = (status: 'pending') => {
  switch (status) {
    case 'pending':
      return 'text-tOrange-400'
  }
}

const getCircleColor = (status: 'pending') => {
  switch (status) {
    case 'pending':
      return 'border-tOrange-400'
  }
}

const TicketStatus = ({ status, statusText }: Props) => {
  const bgColor = getBgColor(status)
  const textColor = getTextColor(status)
  const borderColor = getCircleColor(status)

  const containerClasses = `flex items-center justify-center rounded-full px-4 py-2 ${bgColor}`
  const circleClasses = `ml-2 h-3 w-3 rounded-full border ${borderColor} bg-transparent`

  return (
    <div className={containerClasses}>
      <div className={circleClasses} />
      <p className={`${textColor}`}>{statusText}</p>
    </div>
  )
}

export default TicketStatus

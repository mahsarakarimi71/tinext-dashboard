import { useQuery } from '@tanstack/react-query'
import { getTicketsList } from '../api'
import queryKeys from './queryKeys'

const useTicketList = (page = 1) => {
  return useQuery([queryKeys.getTicketsList, page], () => getTicketsList(page))
}

export default useTicketList

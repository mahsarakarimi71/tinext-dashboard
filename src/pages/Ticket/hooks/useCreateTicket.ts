import { useMutation } from '@tanstack/react-query'
import { createTicket } from '../api'

const useCreateTicket = () => {
  return useMutation<{ message: string }, Error, any, unknown>(
    (data: { title: string; message: string }) => createTicket(data)
  )
}

export default useCreateTicket

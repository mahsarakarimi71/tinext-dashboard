import * as React from 'react'
import * as Yup from 'yup'
import { Formik } from 'formik'
import { Button, ErrorCaption, Heading, Input, Modal } from '@/components'
import { useUserStore } from '@/hooks'
import { useChangePassword } from './hooks'
import { CorporateUsers, DepotsList } from './components'
import { displayToast, PASSWORD_VALIDATION } from '@/lib'

const SCHEMA = Yup.object().shape({
  current_password: Yup.string()
    .matches(PASSWORD_VALIDATION, 'کلمه عبور صحیح نمی باشد')
    .required('لطفا کلمه عبور فعلی را وارد کنید'),
  password: Yup.string()
    .matches(
      PASSWORD_VALIDATION,
      'کلمه عبور باید حداقل 8 کاراکتر باشد و شامل حروف انگلیسی، اعداد و کاراکترهای خاص باشد'
    )
    .required('لطفا کلمه عبور جدید را وارد کنید'),
  password_confirmation: Yup.string()
    .matches(
      PASSWORD_VALIDATION,
      'کلمه عبور باید حداقل 8 کاراکتر باشد و شامل حروف انگلیسی، اعداد و کاراکترهای خاص باشد'
    )
    .oneOf([Yup.ref('password'), null], 'کلمه عبور باید یکسان باشد')
    .required('لطفا تکرار کلمه عبور جدید را وارد کنید')
})

const Profile = () => {
  const [modalStatus, setModalStatus] = React.useState(false)

  const { user } = useUserStore()

  const { mutate, isLoading: changePasswordLoading } = useChangePassword()

  const toggleModalStatus = () => {
    setModalStatus((prev) => !prev)
  }

  const modalOnClose = () => {
    setModalStatus(false)
  }

  return (
    <div>
      <div>
        <div className="mb-10 flex items-center justify-between">
          <Heading title="پروفایل" />
          <Button onClick={toggleModalStatus} type="outlined">
            <Button.Icon icon="ok01" size={18} />
            <Button.Text>تغییر رمز عبور</Button.Text>
          </Button>

          <Modal isOpen={modalStatus}>
            <Modal.Body>
              <Modal.Header title="تغییر رمز عبور" onClose={modalOnClose} />
              <Formik
                initialValues={{
                  current_password: '',
                  password: '',
                  password_confirmation: ''
                }}
                validationSchema={SCHEMA}
                onSubmit={(values, { resetForm }) => {
                  mutate(values, {
                    onSuccess: (d) => {
                      displayToast(d.message, 'success')
                      resetForm()
                    },
                    onError: (e) => displayToast(e.message, 'error')
                  })
                }}
              >
                {({
                  values,
                  errors,
                  touched,
                  handleChange,
                  handleBlur,
                  handleSubmit
                }) => (
                  <form onSubmit={handleSubmit}>
                    <div>
                      <div className="relative my-10">
                        <Input.Label text="رمز عبور فعلی" />
                        <Input.Editor
                          name="current_password"
                          type="password"
                          placeholder="رمز عبور فعلی"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.current_password}
                          model={
                            errors.current_password && touched.current_password
                              ? 'error'
                              : 'normal'
                          }
                          ltr
                        />
                        {errors.current_password &&
                          touched.current_password && (
                            <ErrorCaption>
                              {errors.current_password}
                            </ErrorCaption>
                          )}
                      </div>
                      <div className="relative my-10">
                        <Input.Label text="رمز عبور جدید" />
                        <Input.Editor
                          name="password"
                          type="password"
                          placeholder="رمز عبور جدید"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.password}
                          model={
                            errors.password && touched.password
                              ? 'error'
                              : 'normal'
                          }
                          ltr
                        />
                        {errors.password && touched.password && (
                          <ErrorCaption>{errors.password}</ErrorCaption>
                        )}
                      </div>
                      <div className="relative my-10">
                        <Input.Label text="تکرار رمز عبور جدید" />
                        <Input.Editor
                          name="password_confirmation"
                          type="password"
                          placeholder="تکرار رمز عبور جدید"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={values.password_confirmation}
                          model={
                            errors.password_confirmation &&
                            touched.password_confirmation
                              ? 'error'
                              : 'normal'
                          }
                          ltr
                        />
                        {errors.password_confirmation &&
                          touched.password_confirmation && (
                            <ErrorCaption>
                              {errors.password_confirmation}
                            </ErrorCaption>
                          )}
                      </div>
                    </div>
                    <div className="my-10 flex items-center justify-center">
                      <Button onClick={modalOnClose} type="secondary">
                        <Button.Text>انصراف</Button.Text>
                      </Button>
                      <div className="mr-4">
                        <Button btnType="submit">
                          {changePasswordLoading ? (
                            <Button.Loading />
                          ) : (
                            <Button.Text>تایید</Button.Text>
                          )}
                        </Button>
                      </div>
                    </div>
                  </form>
                )}
              </Formik>
            </Modal.Body>
          </Modal>
        </div>
        <div className="mb-10">
          <div className="flex flex-col items-center justify-center lg:my-10 lg:flex-row">
            <div className="my-4 w-full lg:my-0 lg:ml-4 lg:w-1/3">
              <Input.Label text="نام" model="disable" />
              <Input.Editor
                model="disable"
                disabled
                value={user.first_name || ''}
              />
            </div>
            <div className="my-4 w-full lg:my-0 lg:w-1/3">
              <Input.Label text="نام خانوادگی" model="disable" />
              <Input.Editor
                model="disable"
                disabled
                value={user.last_name || ''}
              />
            </div>
          </div>
          <div className="flex flex-col items-center justify-center lg:flex-row">
            <div className="my-4 w-full lg:my-0 lg:ml-4 lg:w-1/3">
              <Input.Label text="شماره همراه" model="disable" />
              <Input.Editor
                model="disable"
                disabled
                value={user.mobile || ''}
              />
            </div>
            <div className="my-4 w-full lg:my-0 lg:w-1/3">
              <Input.Label text="ایمیل" model="disable" />
              <Input.Editor model="disable" disabled value={user.email || ''} />
            </div>
          </div>
        </div>
        <DepotsList />
        <CorporateUsers />
      </div>
    </div>
  )
}

export default Profile

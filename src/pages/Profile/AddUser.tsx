import * as React from 'react'
import { Formik } from 'formik'
import * as Yup from 'yup'
import { Button, Heading, Input, ErrorCaption } from '@/components'
import { displayToast, PASSWORD_VALIDATION, PHONE_VALIDATION } from '@/lib'
import { useCreateUser, useEditUser } from './hooks'
import { useLocation, useNavigate } from 'react-router-dom'
import { User } from '@/models'
import { useFetchUser } from '../Auth/hooks'

const SCHEMA = Yup.object().shape({
  first_name: Yup.string().required('لطفا نام را وارد کنید'),
  last_name: Yup.string().required('لطفا نام خانوادگی را وارد کنید'),
  email: Yup.string()
    .email('ایمیل صحیح نمی باشد')
    .required('لطفا ایمیل را وارد کنید'),
  mobile: Yup.string()
    .matches(PHONE_VALIDATION, 'شماره همراه صحیح نمی باشد')
    .required('لطفا شماره همراه را وارد کنید'),
  password: Yup.string()
    .matches(
      PASSWORD_VALIDATION,
      'کلمه عبور باید حداقل 8 کاراکتر باشد و شامل حروف انگلیسی، اعداد و کاراکترهای خاص باشد'
    )
    .required('لطفا کلمه عبور را وارد کنید')
})

const EDIT_SCHEMA = Yup.object().shape({
  first_name: Yup.string().required('لطفا نام را وارد کنید'),
  last_name: Yup.string().required('لطفا نام خانوادگی را وارد کنید'),
  email: Yup.string()
    .email('ایمیل صحیح نمی باشد')
    .required('لطفا ایمیل را وارد کنید'),
  mobile: Yup.string()
    .matches(PHONE_VALIDATION, 'شماره همراه صحیح نمی باشد')
    .required('لطفا شماره همراه را وارد کنید')
})

const AddUser = () => {
  const { mutate, isLoading } = useCreateUser()
  const { mutate: editMutate, isLoading: editLoading } = useEditUser()
  const { refetch } = useFetchUser()

  const navigate = useNavigate()

  const location = useLocation()
  const locState = location.state as { user: User }
  const state = locState?.user

  const defaultValues = {
    first_name: state?.first_name || '',
    last_name: state?.last_name || '',
    mobile: state?.mobile || '',
    email: state?.email || '',
    password: ''
  }

  return (
    <div>
      <div className="my-10">
        <Heading title="افزودن کاربر جدید" />
      </div>
      <div>
        <Formik
          initialValues={defaultValues}
          validationSchema={state?.id ? EDIT_SCHEMA : SCHEMA}
          onSubmit={(values, { resetForm }) => {
            if (state?.id) {
              const data = {
                first_name: values.first_name,
                last_name: values.last_name,
                email: values.email,
                mobile: values.mobile,
                id: state?.id,
                role: 3
              }
              editMutate(data, {
                onSuccess: (d) => {
                  refetch()
                  displayToast(d.message, 'success')
                  navigate('/profile', { replace: true })
                },
                onError: (e) => displayToast(e.message, 'error')
              })
            } else {
              mutate(
                {
                  ...values,
                  mobile: `0${values.mobile}`,
                  role: 3
                },
                {
                  onSuccess: (d) => {
                    resetForm()
                    displayToast(d.message, 'success')
                  },
                  onError: (e) => displayToast(e.message, 'error')
                }
              )
            }
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit
          }) => {
            return (
              <form onSubmit={handleSubmit}>
                <div className="my-10 flex flex-col items-center justify-center lg:flex-row">
                  <div className="relative ml-4 w-full lg:w-1/3">
                    <Input.Label text="نام" />
                    <Input.Editor
                      name="first_name"
                      value={values.first_name}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      placeholder="نام"
                      model={
                        touched.first_name && errors.first_name
                          ? 'error'
                          : 'normal'
                      }
                    />
                    {errors.first_name && touched.first_name && (
                      <ErrorCaption>{errors.first_name}</ErrorCaption>
                    )}
                  </div>
                  <div className="relative ml-4 mt-8 w-full lg:mt-0 lg:w-1/3">
                    <Input.Label text="نام خانوادگی" />
                    <Input.Editor
                      name="last_name"
                      value={values.last_name}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      placeholder="نام خانوادگی"
                      model={
                        touched.last_name && errors.last_name
                          ? 'error'
                          : 'normal'
                      }
                    />
                    {errors.last_name && touched.last_name && (
                      <ErrorCaption>{errors.last_name}</ErrorCaption>
                    )}
                  </div>
                </div>
                <div className="my-8 flex flex-col items-center justify-center lg:flex-row">
                  <div className="relative ml-4 w-full lg:w-1/3">
                    <Input.Label text="شماره همراه" type="number" />
                    <Input.Editor
                      type="number"
                      name="mobile"
                      value={values.mobile}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      placeholder="شماره همراه"
                      ltr
                      model={
                        touched.mobile && errors.mobile ? 'error' : 'normal'
                      }
                    />
                    {errors.mobile && touched.mobile && (
                      <ErrorCaption>{errors.mobile}</ErrorCaption>
                    )}
                  </div>
                  <div className="relative ml-4 mt-8 w-full lg:mt-0 lg:w-1/3">
                    <Input.Label text="ایمیل" />
                    <Input.Editor
                      name="email"
                      value={values.email}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      placeholder="ایمیل"
                      type="email"
                      ltr
                      model={touched.email && errors.email ? 'error' : 'normal'}
                    />
                    {errors.email && touched.email && (
                      <ErrorCaption>{errors.email}</ErrorCaption>
                    )}
                  </div>
                </div>
                {!state?.id && (
                  <div className="my-8 flex flex-col items-center justify-center lg:flex-row">
                    <div className="relative ml-4 w-full lg:w-1/3">
                      <Input.Label text="رمز عبور" />
                      <Input.Editor
                        name="password"
                        value={values.password}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        type="password"
                        placeholder="رمز عبور"
                        ltr
                        model={
                          touched.password && errors.password
                            ? 'error'
                            : 'normal'
                        }
                      />
                      {errors.password && touched.password && (
                        <ErrorCaption>{errors.password}</ErrorCaption>
                      )}
                    </div>
                    <div className="ml-4 w-full lg:w-1/3" />
                  </div>
                )}
                <div className="flex w-full justify-end lg:w-5/6">
                  <Button
                    disabled={isLoading || editLoading}
                    btnType="submit"
                    type="primary"
                  >
                    {isLoading || editLoading ? (
                      <Button.Loading />
                    ) : (
                      <Button.Text>
                        {state?.id ? 'ویرایش کاربر' : 'افزودن کاربر جدید'}
                      </Button.Text>
                    )}
                  </Button>
                </div>
              </form>
            )
          }}
        </Formik>
      </div>
    </div>
  )
}

export default AddUser

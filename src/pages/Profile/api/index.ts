import { Api } from '@/lib'
import { Depot, PaginatedResponse, User } from '@/models'

export const getDepotList = (page = 1): Promise<PaginatedResponse<Depot>> => {
  return Api.req('GET', `/api/corporate/v1/depot?page=${page}`)
}

export const getCorporateUsers = (page = 1): Promise<{ data: User[] }> => {
  return Api.req('GET', `/api/corporate/v1/users?page=${page}`)
}

export type Data = {
  current_password: string
  password: string
  password_confirmation: string
}

export const changePassword = (data: Data) => {
  return Api.req('PATCH', `/api/corporate/v1/change-password`, data)
}

export type CreateUserType = {
  first_name: string
  last_name: string
  mobile: string
  email: string
  password: string
  role: number
}

export const createUser = (data: CreateUserType) => {
  return Api.req('POST', '/api/corporate/v1/users', data)
}

export const editUser = (data: Partial<User>) => {
  return Api.req('PATCH', `/api/corporate/v1/users/${data.id}`, data)
}

export const deleteUser = (userId: number) => {
  return Api.req('DELETE', `/api/corporate/v1/users/${userId}`)
}

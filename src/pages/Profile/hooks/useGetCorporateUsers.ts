import { useQuery } from '@tanstack/react-query'
import { getCorporateUsers } from '../api'
import queryKeys from './queryKeys'

const useGetCorporateUsers = (page = 1) => {
  return useQuery([queryKeys.getCorporateUsers, page], () =>
    getCorporateUsers(page)
  )
}

export default useGetCorporateUsers

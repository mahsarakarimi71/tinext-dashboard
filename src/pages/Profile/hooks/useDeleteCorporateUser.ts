import { useMutation } from '@tanstack/react-query'
import { deleteUser } from '../api'

const useDeleteCorporateUser = () => {
  return useMutation<{ message: string }, Error, number, unknown>(
    (userId: number) => deleteUser(userId)
  )
}

export default useDeleteCorporateUser

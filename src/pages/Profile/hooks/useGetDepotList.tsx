import { useQuery } from '@tanstack/react-query'
import { getDepotList } from '../api'
import queryKeys from './queryKeys'

const useGetDepotList = (page = 1) => {
  return useQuery([queryKeys.getDepotList, page], () => getDepotList(page))
}

export default useGetDepotList

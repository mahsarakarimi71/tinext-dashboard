import { User } from '@/models'
import { useMutation } from '@tanstack/react-query'
import { editUser } from '../api'

const useEditUser = () => {
  return useMutation<{ message: string }, Error, Partial<User>, unknown>(
    (data: Partial<User>) => editUser(data)
  )
}

export default useEditUser

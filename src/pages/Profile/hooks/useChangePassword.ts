import { useMutation } from '@tanstack/react-query'
import { changePassword, Data } from '../api'

const useChangePassword = () => {
  return useMutation<{ message: string }, Error, Data, unknown>((data: Data) =>
    changePassword(data)
  )
}

export default useChangePassword

import { useMutation } from '@tanstack/react-query'
import { createUser, CreateUserType } from '../api'

const useCreateUser = () => {
  return useMutation<{ message: string }, Error, CreateUserType, unknown>(
    (data: CreateUserType) => createUser(data)
  )
}

export default useCreateUser

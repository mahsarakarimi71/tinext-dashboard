import * as React from 'react'
import { Heading, Input, MapView, SelectBox } from '@/components'
import { MapRef } from 'react-map-gl'

const AddDepot = () => {
  const map = React.useRef<MapRef | null>(null)

  React.useEffect(() => {
    if (navigator.geolocation) {
      navigator.permissions
        .query({ name: 'geolocation' })
        .then(function (result) {
          if (result.state === 'granted') {
            navigator.geolocation.getCurrentPosition((pos) => {
              console.log(pos, map.current)
              map.current?.flyTo({
                center: [pos.coords.longitude, pos.coords.latitude]
              })
            })
          } else if (result.state === 'prompt') {
            navigator.geolocation.getCurrentPosition(
              (pos) => {
                console.log(pos, map.current)
                map.current?.flyTo({
                  center: [pos.coords.longitude, pos.coords.latitude]
                })
              },
              (err) => console.log(err),
              {
                enableHighAccuracy: true,
                timeout: 5000,
                maximumAge: 0
              }
            )
          }
          result.onchange = function () {
            console.log(result.state)
          }
        })
    }
  }, [])

  return (
    <div className="py-10">
      <div className="mb-10">
        <Heading title="محل جمع‌آوری جدید" />
      </div>
      <div>
        <div className="flex items-center justify-center">
          <div className="ml-4 w-full xl:w-1/4">
            <Input.Label text="عنوان" />
            <Input.Editor placeholder="عنوان" />
          </div>
          <div className="w-full xl:w-1/4">
            <Input.Label text="شهر" />
            <Input.Editor placeholder="شهر" />
          </div>
        </div>
        <div className="mt-10 flex items-center justify-center">
          <div className="w-full xl:w-2/4">
            <Input.Label text="آدرس" />
            <Input.Editor placeholder="آدرس" />
          </div>
        </div>
        <div className="mt-10 flex items-center justify-center">
          <div className="ml-4 w-full xl:w-1/4">
            <Input.Label text="پلاک" />
            <Input.Editor placeholder="پلاک" />
          </div>
          <div className="w-full xl:w-1/4">
            <Input.Label text="واحد" />
            <Input.Editor placeholder="واحد" />
          </div>
        </div>
        <div className="mt-10 flex items-center justify-center">
          <div className="ml-4 w-full xl:w-1/4">
            <Input.Label text="نام مسئول" />
            <Input.Editor placeholder="نام مسئول" />
          </div>
          <div className="w-full xl:w-1/4">
            <Input.Label text="نام خانوادگی مسئول" />
            <Input.Editor placeholder="نام خانوادگی مسئول" />
          </div>
        </div>
        <div className="mt-10 flex flex-col items-center justify-center lg:flex-row">
          <div className="w-full lg:ml-4 xl:w-1/4">
            <Input.Label text="شماره همراه مسئول" />
            <Input.Editor placeholder="شماره همراه مسئول" type="number" />
          </div>
          <div className="mt-10 w-full lg:mt-0 xl:w-1/4">
            <Input.Label text="حالت" />
            <SelectBox
              options={[
                { value: 'سلام', label: 'سلام' },
                { value: 'چطوری', label: 'چطوری' }
              ]}
              onSelect={console.log}
              placeholder="پیشفرض / غیرفعال / غیر پیشفرض"
            />
          </div>
        </div>
        <div className="flex flex-col items-center justify-center">
          <div className="mt-10 w-full xl:w-2/4">
            <p className="my-6">انتخاب از روی نقشه</p>
            <MapView ref={map} />
          </div>
        </div>
      </div>
    </div>
  )
}

export default AddDepot

import * as React from 'react'
import {
  Heading,
  Button,
  CenterLoading,
  Table,
  ColumnType,
  MobileTable,
  IndexIndicator,
  Pagination,
  Icon
} from '@/components'
import { Depot } from '@/models'
import { useNavigate } from 'react-router-dom'
import { useGetDepotList } from '../hooks'

const DepotsList = () => {
  const [page, setPage] = React.useState(1)

  const { isLoading: depotLoading, data: depotData } = useGetDepotList(page)

  const depotColumns: ColumnType[] = [
    {
      label: 'نام',
      accessor: 'label',
      sortable: true
    },
    {
      label: 'شهر',
      accessor: 'x',
      sortable: true
    },
    {
      label: 'آدرس',
      accessor: 'address',
      sortable: false
    },
    {
      label: 'واحد',
      accessor: 'unit_no',
      sortable: false
    },
    {
      label: 'پلاک',
      accessor: 'plate_no',
      sortable: false
    },
    {
      label: ' ',
      accessor: 'default_depot',
      sortable: false,
      Component: ({ rowData }: { rowData: Depot }) => {
        if (rowData.default_depot) {
          return <DefaultDepotIndicator />
        }
        return null
      }
    },
    {
      label: 'عملیات',
      accessor: 'xyz',
      sortable: false,
      Component: ({ rowData }: { rowData: Depot }) => {
        return (
          <div className="flex items-center justify-center">
            <button className="ml-4">
              <Icon icon="sabte-sefaresh-1002" size={18} />
            </button>
            <button>
              <Icon icon="sabte-sefaresh-1003" size={18} />
            </button>
          </div>
        )
      }
    }
  ]

  const navigate = useNavigate()

  const onPageChanged = (pageNumber: number) => {
    setPage(pageNumber)
  }

  const navigateToAddDepot = () => {
    navigate('/profile/add-depot')
  }

  return (
    <div className="my-20">
      <div className="flex items-center justify-between">
        <Heading title="محل‌های جمع‌آوری" />
        <Button onClick={navigateToAddDepot} type="outlined">
          <Button.Icon icon="Tinext-Panel06" size={18} />
          <Button.Text>افزودن محل جدید</Button.Text>
        </Button>
      </div>
      <div className="my-10">
        {depotLoading ? (
          <CenterLoading />
        ) : (
          <Table
            data={depotData?.data || []}
            columns={depotColumns}
            page={page}
          />
        )}
        <div className="lg:hidden">
          {depotData?.data.map((depot: Depot, index: number) => (
            <DepotMobileViewItem
              item={depot}
              index={(page - 1) * 15 + index + 1}
              key={depot.id}
            />
          ))}
        </div>
        <Pagination
          currentPage={page}
          totalCount={depotData?.meta.pagination.total || 0}
          totalPages={depotData?.meta.pagination.total_pages || 0}
          onPageChanged={onPageChanged}
        />
      </div>
    </div>
  )
}

const DefaultDepotIndicator = () => (
  <div className="rounded-full bg-primary-100 px-4 py-2 text-primary-500">
    پیشفرض
  </div>
)

const DepotMobileViewItem = ({
  item,
  index
}: {
  item: Depot
  index: number
}) => {
  return (
    <MobileTable>
      <MobileTable.Row justify="between">
        <div className="flex items-center">
          <IndexIndicator index={index} withMargin={false} />
          <p className="mr-2 text-sm font-bold text-masterBlack">
            {item.label}
          </p>
        </div>
        <p>تهران</p>
      </MobileTable.Row>
      <MobileTable.Row justify="between">
        <p className="text-sm text-masterBlack">{item.address}</p>
      </MobileTable.Row>
      <MobileTable.Row justify="start">
        <p className="ml-4">واحد :‌{item.unit_no}</p>
        <p>پلاک : {item.plate_no}</p>
      </MobileTable.Row>
      <MobileTable.Row justify="between">
        {item.default_depot ? <DefaultDepotIndicator /> : <div />}
        <div className="flex items-center justify-center">
          <button className="ml-4">
            <Icon icon="sabte-sefaresh-1002" size={18} />
          </button>
          <button>
            <Icon icon="sabte-sefaresh-1003" size={18} />
          </button>
        </div>
      </MobileTable.Row>
    </MobileTable>
  )
}

export default DepotsList

import * as React from 'react'
import {
  Heading,
  Button,
  CenterLoading,
  MobileTable,
  IndexIndicator,
  ColumnType,
  Modal,
  Table,
  Icon
} from '@/components'
import { useDeleteCorporateUser, useGetCorporateUsers } from '../hooks'
import { User } from '@/models'
import { useNavigate } from 'react-router-dom'
import { displayToast } from '@/lib'

type State = {
  modalStatus: boolean
  id?: number | null
  type?: 'user' | 'depot' | null
}

const CorporateUsers = () => {
  const [modalData, setModalData] = React.useState<State>({
    modalStatus: false,
    id: null,
    type: null
  })

  const {
    isLoading: userLoading,
    data: userData,
    refetch: refetchUsers,
    isRefetching: isRefetchingUsers
  } = useGetCorporateUsers(1)

  const { mutate: deleteMutate, isLoading: deleteUserLoading } =
    useDeleteCorporateUser()

  const navigate = useNavigate()

  const userColumns: ColumnType[] = [
    {
      label: 'نام',
      accessor: 'first_name',
      sortable: true
    },
    {
      label: 'نام خانوادگی',
      accessor: 'last_name',
      sortable: true
    },
    {
      label: 'شماره همراه',
      accessor: 'mobile',
      sortable: false
    },
    {
      label: 'ایمیل',
      accessor: 'email',
      sortable: false
    },
    {
      label: 'سطح دسترسی',
      accessor: 'plate_no',
      sortable: true
    },
    {
      label: 'عملیات',
      accessor: 'xyz',
      sortable: false,
      Component: ({ rowData }: { rowData: User }) => {
        return (
          <div className="flex items-center justify-center">
            <button className="ml-4" onClick={() => editUser(rowData)}>
              <Icon icon="sabte-sefaresh-1002" size={18} />
            </button>
            <button onClick={() => userDeleteOnClick(rowData.id)}>
              <Icon icon="sabte-sefaresh-1003" size={18} />
            </button>
          </div>
        )
      }
    }
  ]

  const userDeleteOnClick = (id: number) => {
    setModalData({
      modalStatus: true,
      id
    })
  }

  const deleteUser = () => {
    if (modalData.id) {
      deleteMutate(modalData.id, {
        onSuccess: (d) => {
          setModalData({
            modalStatus: false,
            id: null,
            type: null
          })
          displayToast(d.message, 'success')
          refetchUsers()
        },
        onError: (e) => displayToast(e.message, 'error')
      })
    }
  }

  const toggleDeleteModal = (id?: number) => {
    setModalData((prev) => ({
      modalStatus: !prev.modalStatus,
      id
    }))
  }

  const navigateToAddUser = () => {
    navigate('/profile/add-user')
  }

  const editUser = (user: User) => {
    navigate('/profile/add-user', {
      state: {
        user
      }
    })
  }

  return (
    <div className="my-20">
      <div className="flex items-center justify-between">
        <Heading title="کاربران" />
        <Button onClick={navigateToAddUser} type="outlined">
          <Button.Icon icon="Tinext-Panel06" size={18} />
          <Button.Text>افزودن کاربر جدید</Button.Text>
        </Button>
      </div>
      <div className="my-10">
        {userLoading || isRefetchingUsers ? (
          <CenterLoading />
        ) : (
          <>
            <Table data={userData?.data || []} columns={userColumns} page={1} />
            <div className="lg:hidden">
              {userData?.data.map((user: User, index: number) => (
                <UserMobileViewItem
                  item={user}
                  key={user.id}
                  index={index + 1}
                  editOnClick={editUser}
                  deleteOnClick={toggleDeleteModal}
                />
              ))}
            </div>
          </>
        )}
      </div>
      <Modal isOpen={modalData.modalStatus}>
        <Modal.Body>
          <Modal.Header onClose={toggleDeleteModal} title={'حذف کاربر'} />
          <div className="my-10">
            <h3 className="text-lg font-bold">
              آیا از حذف {'کاربر'} اطمینان دارید؟
            </h3>
          </div>
          <div className="my-10 flex items-center justify-center">
            <Button type="secondary" onClick={() => toggleDeleteModal()}>
              <Button.Text>انصراف</Button.Text>
            </Button>
            <Button onClick={deleteUser}>
              {deleteUserLoading ? (
                <Button.Loading />
              ) : (
                <Button.Text>تایید</Button.Text>
              )}
            </Button>
          </div>
        </Modal.Body>
      </Modal>
    </div>
  )
}

type MobileViewProps = {
  item: User
  index: number
  editOnClick: (user: User) => void
  deleteOnClick: (id: number) => void
}

const UserMobileViewItem = ({
  item,
  index,
  editOnClick,
  deleteOnClick
}: MobileViewProps) => {
  return (
    <MobileTable>
      <MobileTable.Row justify="between">
        <div className="flex items-center">
          <IndexIndicator index={index} withMargin={false} />
          <p className="mr-2 text-sm font-bold text-masterBlack">
            {item.full_name}
          </p>
        </div>
        <p>{item.mobile}</p>
      </MobileTable.Row>
      <MobileTable.Row justify="between">
        <p className="text-sm text-masterBlack">
          ایمیل: <span className="font-bold">{item.email}</span>
        </p>
      </MobileTable.Row>
      <MobileTable.Row justify="between">
        <div />
        <div className="flex items-center justify-center">
          <button onClick={() => editOnClick(item)} className="ml-4">
            <Icon icon="sabte-sefaresh-1002" size={18} />
          </button>
          <button onClick={() => deleteOnClick(item.id)}>
            <Icon icon="sabte-sefaresh-1003" size={18} />
          </button>
        </div>
      </MobileTable.Row>
    </MobileTable>
  )
}

export default CorporateUsers

import * as React from 'react'
import { Button, Input, AuthLayout } from '@/components'
import { useLocation, useNavigate } from 'react-router-dom'

type State = {
  email: string
  title: string
}

const Verification = () => {
  const [timer, setTimer] = React.useState(180)
  const inputRefs = React.useRef<HTMLInputElement[]>([])
  const navigate = useNavigate()
  const location = useLocation()

  const { email, title } = location.state as State

  const min = Math.floor(timer / 60)
  const sec = timer - min * 60

  React.useEffect(() => {
    const interval = setInterval(() => {
      if (timer > 0) {
        setTimer((t) => t - 1)
      } else {
        clearInterval(interval)
      }
    }, 1000)
    return () => clearInterval(interval)
  }, [])

  const setRefs = (ref: HTMLInputElement) => {
    inputRefs.current.push(ref)
  }

  const onChange = (e: React.FormEvent<HTMLInputElement>) => {
    const inputNo = parseInt(e.currentTarget.dataset.no || '0', 10)
    const nextInput = inputNo + 1
    if (inputRefs.current[nextInput]) {
      inputRefs.current[nextInput].focus()
    }
  }

  const goToRegistration = () => {
    navigate('/register/details')
  }

  return (
    <AuthLayout title={title}>
      <div>
        <div className="mt-20 px-4 2xl:px-12">
          <div className="w-full content-between px-5">
            <Input.Label text="ایمیل شما" model="disable" />
            <Input.Editor
              ltr
              model="disable"
              value={email}
              placeholder="شماره موبایل یا ایمیل"
            />
          </div>
          <div className="mt-12 w-full px-5">
            <p>کد ارسال شده به ایمیل را اینجا وارد کنید</p>
            <div className="my-8 flex w-full flex-row-reverse items-center justify-between">
              {Array(5)
                .fill(0)
                .map((_, index) => {
                  return (
                    <div key={index} className="mx-1 md:mx-4">
                      <Input.Editor
                        classes="text-center"
                        maxLength={1}
                        type="number"
                        ref={setRefs}
                        onChange={onChange}
                        data-no={index}
                      />
                    </div>
                  )
                })}
            </div>
          </div>
          <div className="flex items-center justify-center text-primary-400">
            <p className="ml-4">{`${min}:${sec}`}</p>
            <button className="disabled:opacity-70" disabled={timer > 0}>
              ارسال مجدد
            </button>
          </div>
        </div>
        <div className="mt-20 flex justify-center">
          <Button onClick={goToRegistration}>
            <Button.Text>مرحله بعد</Button.Text>
          </Button>
        </div>
      </div>
    </AuthLayout>
  )
}

export default Verification

import * as React from 'react'
import { AuthLayout, Button } from '@/components'
import { useNavigate } from 'react-router-dom'

const Contract = () => {
  const [signed, setSigned] = React.useState(false)

  const navigate = useNavigate()

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSigned(e.currentTarget.checked)
  }

  const navigateToDashboard = () => {
    navigate('/')
  }

  return (
    <AuthLayout hideBack title="قرارداد همکاری">
      <div className="px-8">
        <div>
          <div className="mt-12 flex items-center">
            <div className="ml-2 h-3 w-3 bg-primary-400" />
            <h2 className="text-lg text-masterBlack">ماده ۱: تعاریف</h2>
          </div>
          <p className="mt-4 max-w-[700px] leading-8 text-masterBlack">
            فرستنده: منظور کاربری است که مطابق با شرایط همکاری مندرج در قرارداد٬
            برای جابه جایی مرسوله سفارش گذاری می نماید.
          </p>
        </div>
        <div>
          <div className="mt-12 flex items-center">
            <div className="ml-2 h-3 w-3 bg-primary-400" />
            <h2 className="text-lg text-masterBlack">ماده ۲: موضوع</h2>
          </div>
          <p className="mt-4 max-w-[700px] leading-8 text-masterBlack">
            جمع‌آوری و توزیع مرسوله‌های فرستنده مطابق با شرایط و پیوست‌های
            قرارداد
          </p>
        </div>
        <div>
          <div className="mt-12 flex items-center">
            <div className="ml-2 h-3 w-3 bg-primary-400" />
            <h2 className="text-lg text-masterBlack">ماده ۳: مدت همکاری</h2>
          </div>
          <p className="mt-4 max-w-[700px] leading-8 text-masterBlack">
            پذیرش قرارداد توسط فرستنده به منزله شروع همکاری بوده و مادامی که
            فرستنده اقدام به سفارش گذاری نماید٬ قرارداد پابرجاست.
          </p>
        </div>
        <div>
          <div className="mt-12 flex items-center">
            <div className="ml-2 h-3 w-3 bg-primary-400" />
            <h2 className="text-lg text-masterBlack">
              ماده ۴: تعهدات و مسئولیت‌های فرستنده
            </h2>
          </div>
          <p className="mt-4 max-w-[700px] leading-8 text-masterBlack">
            صحت سنجی و بررسی اعتبار آدرس گیرندگان بر عهده فرستنده بوده و در صورت
            ارائه آدرس اشتباه٬ هزینه‌های حمل دریافت می‌گردد. فرستنده می‌بایست
            لیبل‌های تی‌نکست که شامل بارکد است را روی تمامی مرسوله‌های خود نصب
            نماید٬ در غیر این‌صورت تی‌نکست مرسوله‌ها را دریافت نمی‌کند.
          </p>
        </div>
      </div>
      <div className="mt-8 flex items-center px-8">
        <input
          name="acceptance"
          type="checkbox"
          className="ml-2 h-4 w-4"
          onChange={onChange}
        />
        <label htmlFor="acceptance" className="text-sm text-masterBlack">
          قرارداد همکاری را مطالعه و موافق هستم
        </label>
      </div>
      <div className="mt-20 flex justify-center">
        <Button onClick={navigateToDashboard} disabled={!signed}>
          <Button.Text>مرحله بعد</Button.Text>
        </Button>
      </div>
    </AuthLayout>
  )
}

export default Contract

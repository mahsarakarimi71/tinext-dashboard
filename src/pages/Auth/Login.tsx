import * as React from 'react'
import { Link, useLocation, useNavigate } from 'react-router-dom'
import { Button, Input, AuthLayout } from '@/components'
import { useLogin } from './hooks'
import { cookieHandler, displayToast } from '@/lib'

type LocationState = {
  phoneOrEmail: string
}

const Login = () => {
  const [password, setPassword] = React.useState('')

  const location = useLocation()
  const navigate = useNavigate()

  const { mutate, isLoading } = useLogin()

  const { phoneOrEmail } = location.state as LocationState

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(e.currentTarget.value)
  }

  const doLogin = () => {
    mutate(
      {
        email: phoneOrEmail,
        password
      },
      {
        onSuccess: (d) => {
          cookieHandler.setCookie('jwt_token', d.access_token)
          navigate('/', { replace: true })
        },
        onError: (e) => displayToast(e.message, 'error')
      }
    )
  }

  return (
    <AuthLayout title="ورود">
      <div>
        <div className="mt-20 px-4 2xl:px-12">
          <div className="w-full content-between px-5">
            <Input.Label text="شماره موبایل یا ایمیل" model="disable" />
            <Input.Editor
              ltr
              model="disable"
              value={phoneOrEmail}
              placeholder="شماره موبایل یا ایمیل"
            />
          </div>
          <div className="mt-12 w-full content-between px-5">
            <Input.Label text="رمز عبور" model="normal" />
            <Input.Editor
              ltr
              model="normal"
              type="password"
              placeholder="رمز عبور"
              value={password}
              onChange={onChange}
            />
          </div>
          <div className="mt-12 flex justify-end pl-8">
            <Link to="/forgot-password">
              <button className="text-masterBlack hover:text-primary-400">
                رمز عبور خود را فراموش کردید؟
              </button>
            </Link>
          </div>
        </div>
        <div className="mt-20 flex justify-center">
          <Button onClick={doLogin} disabled={password.length < 6}>
            {isLoading ? (
              <Button.Loading />
            ) : (
              <>
                <Button.Text>مرحله بعد</Button.Text>
                <Button.Icon icon="login01" />
              </>
            )}
          </Button>
        </div>
      </div>
    </AuthLayout>
  )
}

export default Login

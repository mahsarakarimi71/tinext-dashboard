import { useUserStore } from '@/hooks'
import { useQuery } from '@tanstack/react-query'
import { fetchUser } from '../api'
import queryKeys from './queryKeys'

const useFetchUser = () => {
  const data = useQuery([queryKeys.fetchUser], fetchUser, {
    onSuccess: (response) => {
      useUserStore.getState().updateUser(response)
    },
    retry: 0
  })
  return data
}

export default useFetchUser

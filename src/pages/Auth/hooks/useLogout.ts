import { useMutation } from '@tanstack/react-query'
import { logout } from '../api'

const useLogout = () => {
  return useMutation(logout)
}

export default useLogout

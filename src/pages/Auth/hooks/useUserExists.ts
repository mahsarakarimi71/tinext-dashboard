import { IsExistResponse } from '@/models'
import { useMutation } from '@tanstack/react-query'
import { checkIfUserExists } from '../api'

const useUserExists = () => {
  const data = useMutation<IsExistResponse, Error, string, unknown>(
    (data: string) => checkIfUserExists({ email_phone: data })
  )
  return data
}

export default useUserExists

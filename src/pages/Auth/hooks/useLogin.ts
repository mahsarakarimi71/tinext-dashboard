import { LoginResponse } from '@/models'
import { useMutation } from '@tanstack/react-query'
import { login, LoginData } from '../api'

const useLogin = () => {
  const data = useMutation<LoginResponse, Error, LoginData, unknown>(
    (data: LoginData) => login(data)
  )
  return data
}

export default useLogin

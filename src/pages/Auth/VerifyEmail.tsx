import { Button, Input, AuthLayout } from '@/components'
import { useNavigate } from 'react-router-dom'

const VerifyEmail = () => {
  const navigate = useNavigate()

  const goToVerification = () => {
    navigate('/register/verification', {
      state: {
        email: 'email@email.com',
        title: 'ثبت نام کاربر جدید'
      }
    })
  }

  return (
    <AuthLayout title="ثبت نام کاربر جدید">
      <div>
        <div className="mt-20 px-4 2xl:px-12">
          <div className="w-full content-between px-5">
            <Input.Label text="ایمیل شما" model="disable" />
            <Input.Editor
              ltr
              model="disable"
              value="email@gmail.com"
              placeholder="شماره موبایل یا ایمیل"
            />
          </div>
          <div className="mt-12 flex w-full content-between items-center justify-between px-5">
            <div className="flex-1">
              <Input.Label text="کد امنیتی" model="normal" />
              <Input.Editor
                ltr
                model="normal"
                type="number"
                placeholder="کد امنیتی"
              />
            </div>
            <div className="flex-1" />
          </div>
        </div>
        <div className="mt-20 flex justify-center">
          <Button onClick={goToVerification}>
            <Button.Text>دریافت کد تایید</Button.Text>
          </Button>
        </div>
      </div>
    </AuthLayout>
  )
}

export default VerifyEmail

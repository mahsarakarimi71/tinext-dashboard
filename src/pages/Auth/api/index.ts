import { Api } from '@/lib'
import { IsExistResponse, LoginResponse } from '@/models'

export const checkIfUserExists = (data: {
  email_phone: string
}): Promise<IsExistResponse> => {
  return Api.req('POST', '/api/corporate/v1/is-exist-user', data)
}

export type LoginData = {
  email: string
  password: string
}

export const login = (data: LoginData): Promise<LoginResponse> => {
  return Api.req('POST', '/api/corporate/v1/login', data)
}

export const logout = () => {
  return Api.req('POST', '/api/corporate/v1/logout')
}

export const fetchUser = () => {
  return Api.req('GET', '/api/corporate/v1/me')
}

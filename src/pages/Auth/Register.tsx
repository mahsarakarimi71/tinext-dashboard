import { Button, Input, AuthLayout } from '@/components'
import { Link } from 'react-router-dom'

const Register = () => {
  return (
    <AuthLayout title="ثبت نام کاربر جدید">
      <div>
        <div className="mt-20 px-4 2xl:px-12">
          <div className="grid grid-cols-1 gap-10 md:grid-cols-2 md:gap-0">
            <div className="px-5">
              <Input.Label text="نام" model="normal" />
              <Input.Editor model="normal" placeholder="نام" />
            </div>
            <div className="px-5">
              <Input.Label text="نام خانوادگی" model="normal" />
              <Input.Editor model="normal" placeholder="نام خانوادگی" />
            </div>
          </div>
          <div className="my-10 grid grid-cols-1">
            <div className="px-5">
              <Input.Label text="آدرس" model="normal" />
              <Input.Editor model="normal" placeholder="آدرس" />
            </div>
          </div>
          <div className="my-10 grid grid-cols-1 gap-10 md:grid-cols-2 md:gap-0">
            <div className="px-5">
              <Input.Label text="شماره ملی" model="normal" />
              <Input.Editor
                model="normal"
                type="number"
                placeholder="شماره ملی"
                ltr
              />
            </div>
            <div className="px-5">
              <Input.Label text="تلفن" model="normal" />
              <Input.Editor
                model="normal"
                type="number"
                ltr
                placeholder="تلفن"
              />
            </div>
          </div>
          <div className="my-10 grid grid-cols-1 gap-10 md:grid-cols-2 md:gap-0">
            <div className="px-5">
              <Input.Label text="موبایل" model="normal" />
              <Input.Editor
                model="normal"
                type="number"
                placeholder="موبایل"
                ltr
              />
            </div>
            <div className="px-5">
              <Input.Label text="ایمیل" model="normal" />
              <Input.Editor
                model="normal"
                type="email"
                ltr
                placeholder="ایمیل"
              />
            </div>
          </div>
          <div className="my-10 grid grid-cols-1 gap-10 md:grid-cols-2 md:gap-0">
            <div className="px-5">
              <Input.Label text="نوع مشتری" model="normal" />
              <Input.Editor
                model="normal"
                type="text"
                placeholder="نوع مشتری"
              />
            </div>
            <div className="px-5">
              <Input.Label text="حوزه فعالیت" model="normal" />
              <Input.Editor
                model="normal"
                type="text"
                placeholder="حوزه فعالیت"
              />
            </div>
          </div>
          <div className="my-10 grid grid-cols-1 gap-10 lg:grid-cols-2 lg:gap-0">
            <div className="px-5">
              <Input.Label text="پیشبینی دفعات سفارش در هفته" model="normal" />
              <Input.Editor
                model="normal"
                type="number"
                placeholder="تعداد"
                ltr
              />
            </div>
            <div className="px-5">
              <Input.Label
                text="پیشبینی تعداد مرسوله در هر سفارش"
                model="normal"
              />
              <Input.Editor
                model="normal"
                type="number"
                ltr
                placeholder="تعداد"
              />
            </div>
          </div>
          <div className="my-10 grid grid-cols-1 gap-10 md:grid-cols-2 md:gap-0">
            <div className="px-5">
              <Input.Label text="رمز عبور" model="normal" />
              <Input.Editor
                model="normal"
                type="password"
                placeholder="رمز عبور"
                ltr
              />
            </div>
            <div className="px-5">
              <Input.Label text="تکرار رمز عبور" model="normal" />
              <Input.Editor
                model="normal"
                type="password"
                ltr
                placeholder="تکرار رمز عبور"
              />
            </div>
          </div>
        </div>
        <div className="mt-20 flex justify-center">
          <Link to="/register/contract">
            <Button>
              <Button.Text>ثبت نام</Button.Text>
            </Button>
          </Link>
        </div>
      </div>
    </AuthLayout>
  )
}

export default Register

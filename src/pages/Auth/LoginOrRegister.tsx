import * as React from 'react'
import { Button, Input, AuthLayout } from '@/components'
import { useUserExists } from './hooks'
import { displayToast } from '@/lib'
import { useNavigate } from 'react-router-dom'

const LoginOrRegister = () => {
  const [data, setData] = React.useState('')
  const { mutate, isLoading } = useUserExists()

  const navigate = useNavigate()

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setData(e.target.value)
  }

  const checkUserExists = () => {
    mutate(data, {
      onSuccess: (d) => {
        if (d.is_exists) {
          navigate('/login', {
            state: {
              phoneOrEmail: data
            }
          })
        }
      },
      onError: (e) => displayToast(e.message, 'error')
    })
  }

  return (
    <AuthLayout isFirstStep title="ورود / ثبت نام">
      <div>
        <div className="mt-20 px-4 2xl:px-12">
          <div className="w-full content-between px-5">
            <Input.Label text="شماره موبایل یا ایمیل" model="normal" />
            <Input.Editor
              model="normal"
              ltr
              onChange={onChange}
              placeholder="شماره موبایل یا ایمیل"
              value={data}
            />
          </div>
        </div>
        <div className="mt-20 flex justify-center">
          <Button onClick={checkUserExists} disabled={!data.length}>
            {isLoading ? (
              <Button.Loading />
            ) : (
              <>
                <Button.Text>مرحله بعد</Button.Text>
                <Button.Icon icon="login01" size={14} />
              </>
            )}
          </Button>
        </div>
      </div>
    </AuthLayout>
  )
}

export default LoginOrRegister

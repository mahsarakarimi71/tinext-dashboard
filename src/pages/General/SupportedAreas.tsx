import { Button, Heading, Input, MapView } from '@/components'

const SupportedAreas = () => {
  return (
    <div className="py-10">
      <div className="mb-10">
        <Heading title="محدوده سرویس" />
      </div>

      <div className="flex items-center justify-center">
        <div className="flex w-2/4">
          <Input.Editor placeholder="جستجوی منطقه یا آدرس" />
          <Button type="outlined">
            <Button.Text>بررسی</Button.Text>
            <Button.Icon icon="Tinext-Panel15" />
          </Button>
        </div>
      </div>

      <div className="my-20">
        <MapView zoom={9} withMarker={false}></MapView>
      </div>
    </div>
  )
}
export default SupportedAreas

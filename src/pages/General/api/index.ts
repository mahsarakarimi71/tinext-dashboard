import { Api } from '@/lib'

export const getChartData = () => {
  return Api.req('GET', '/api/corporate/v1/general/load-chart-data')
}

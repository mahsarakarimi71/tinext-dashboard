import { useChartData } from './hooks'

const Dashboard = () => {
  const { data } = useChartData()

  console.log('======> ', data)

  return <div>This is Dashboard page.</div>
}

export default Dashboard

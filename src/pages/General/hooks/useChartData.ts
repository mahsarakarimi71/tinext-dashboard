import { useQuery } from '@tanstack/react-query'
import { getChartData } from '../api'
import queryKeys from './queryKeys'

const useChartData = () => {
  return useQuery([queryKeys.getChartData], getChartData)
}

export default useChartData

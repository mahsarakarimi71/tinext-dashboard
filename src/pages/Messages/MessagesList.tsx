import { CenterLoading } from '@/components'
import useGetMessages from './hooks/useGetMessages'

const MessagesList = () => {
  const { isLoading, data } = useGetMessages()

  console.log(data)

  if (isLoading) {
    return <CenterLoading />
  }

  return <div>This is MessagesList page.</div>
}

export default MessagesList

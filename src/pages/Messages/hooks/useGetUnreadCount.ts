import { useUnreadCountStore } from '@/hooks'
import { useQuery } from '@tanstack/react-query'
import { getUnreadCount } from '../api'
import queryKeys from './queryKeys'

const useGetUnreadCount = () => {
  return useQuery([queryKeys.getUnreadCount], getUnreadCount, {
    onSuccess: (data) => {
      useUnreadCountStore.getState().setUnreadCount(data.unread_messages_count)
    }
  })
}

export default useGetUnreadCount

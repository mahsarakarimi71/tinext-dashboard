import { useQuery } from '@tanstack/react-query'
import { getMessages } from '../api'
import queryKeys from './queryKeys'

const useGetMessages = () => {
  return useQuery([queryKeys.getMessages], getMessages)
}

export default useGetMessages

import { Api } from '@/lib'
import { UnreadCountResponse } from '@/models'

export const getUnreadCount = (): Promise<UnreadCountResponse> => {
  return Api.req('GET', '/api/corporate/v1/messages/unread-count')
}

export const getMessages = () => {
  return Api.req('GET', '/api/corporate/v1/messages/list')
}

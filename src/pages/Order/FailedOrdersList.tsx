import * as React from 'react'
import {
  Table,
  ColumnType,
  CenterLoading,
  MobileTable,
  IndexIndicator,
  Pagination,
  Heading,
  Button,
  Search,
  Icon
} from '@/components'
import { RefOrder } from '@/models'
import { convertDateToPersian, currencyFormat } from '@/lib'
import { useGetFailedOrders } from './hooks'

const FailedOrdersList = () => {
  const [page, setPage] = React.useState(1)
  const [filter, setFilter] = React.useState('')

  const { data, isLoading } = useGetFailedOrders(page, filter)

  const columns: ColumnType[] = [
    {
      label: 'کد مرسوله',
      accessor: 'code',
      sortable: true
    },
    {
      label: 'کد داخلی',
      accessor: 'id',
      sortable: true
    },
    {
      label: 'نام',
      accessor: 'corporate_name',
      sortable: true
    },
    {
      label: 'شماره موبایل',
      accessor: 'corporate_phone_no',
      sortable: true
    },
    {
      label: 'آدرس گیرنده',
      accessor: 'corporate_address',
      sortable: false
    },
    {
      label: 'مبلغ پرداخت در محل (ریال)',
      accessor: 'price_formatted',
      sortable: true
    },
    {
      label: 'زمان حضور در محل',
      accessor: 'pickup_time',
      sortable: true
    },
    {
      label: 'علت تحویل ناموفق',
      accessor: 'fa_backoffie_status',
      sortable: true
    },
    {
      label: 'توضیح',
      accessor: 'corporate_desc',
      sortable: true
    },
    {
      label: 'عملیات',
      accessor: 'operation',
      sortable: false,
      Component: ({ rowData }: { rowData: RefOrder }) => (
        <div className="flex items-center justify-center">
          <button
            onClick={() => console.log('btnClicked', rowData.id)}
            className="my-2 ml-2"
          >
            <Icon icon="Tinext-Panel08" size={18} />
          </button>
          <button
            onClick={() => console.log('btnClicked', rowData.id)}
            className="my-2 ml-2"
          >
            <Icon icon="register01" size={18} />
          </button>
        </div>
      )
    }
  ]

  const onPageChanged = (pageNumber: number) => {
    setPage(pageNumber)
  }

  const onSearch = (value: string) => {
    setFilter(value)
  }

  return (
    <div>
      <div className="mb-8">
        <Heading title={'تحویل‌های ناموفق'} />
      </div>
      <div className="mb-10">
        <Search
          searchFields={[{ label: 'کد مرسوله', key: 'code', type: 'text' }]}
          onSearch={onSearch}
        />
      </div>
      {isLoading ? (
        <CenterLoading />
      ) : (
        <>
          <div className="mb-10 flex items-center justify-between">
            <h3 className="text-lg font-bold">{data?.meta.total} مورد</h3>
            <Button type="outlined">
              <Button.Text>دریافت خروجی اکسل</Button.Text>
            </Button>
          </div>
          <Table
            data={data?.data || []}
            columns={columns}
            // buttons={operationButtons}
            page={page}
          />
          <div className="lg:hidden">
            {data?.data.map((item, index) => {
              return (
                <MobileViewItem
                  item={item}
                  key={item.id}
                  index={(page - 1) * 15 + index + 1}
                />
              )
            })}
          </div>
          <Pagination
            currentPage={data?.meta.current_page || 0}
            totalPages={data?.meta.total_pages || 0}
            totalCount={data?.meta.total || 0}
            onPageChanged={onPageChanged}
          />
        </>
      )}
    </div>
  )
}

const MobileViewItem = ({ item, index }: { item: RefOrder; index: number }) => {
  return (
    <MobileTable>
      <MobileTable.Row justify="between">
        <div className="flex items-center">
          <IndexIndicator index={index} withMargin={false} />
          <p className="mr-2 text-sm text-masterBlack">{item.code}</p>
        </div>
        <p className="text-sm text-masterBlack">
          کد داخلی: <span className="mr-1 text-base font-bold">{item.id}</span>
        </p>
      </MobileTable.Row>
      <MobileTable.Row justify="between">
        <p className="text-sm text-masterBlack">{item.corporate.name}</p>
        <p className="text-sm text-masterBlack">{item.corporate.phone_no}</p>
      </MobileTable.Row>
      <MobileTable.Row justify="between">
        <p className="text-sm text-masterBlack">{item.corporate.address}</p>
      </MobileTable.Row>
      <MobileTable.Row justify="between">
        <p className="text-sm text-masterBlack">
          مبلغ پرداخت در محل (ریال):
          <span className="mr-1 text-base font-bold">
            {currencyFormat(item.cash_on_delivery)}
          </span>
        </p>
      </MobileTable.Row>
      <MobileTable.Row justify="between">
        <p className="text-sm text-masterBlack">
          زمان حضور در محل:
          <span className="mr-1 text-base font-bold">
            {convertDateToPersian(item.pickup_at.replace(' ', 'T'))} -{' '}
            {item.pickup_timespan}
          </span>
        </p>
      </MobileTable.Row>
      <MobileTable.Row justify="between">
        <p className="text-sm text-masterBlack">
          علت تحویل ناموفق:
          <span className="mr-1 text-base font-bold">
            {item.fa_backoffie_status}
          </span>
        </p>
      </MobileTable.Row>
      {item.corporate.description && (
        <MobileTable.Row justify="between">
          <p className="text-sm text-masterBlack">
            توضیح:
            <span className="mr-1 text-base font-bold">
              {item.corporate.description}
            </span>
          </p>
        </MobileTable.Row>
      )}
    </MobileTable>
  )
}

export default FailedOrdersList

import { Api } from '@/lib'
import { RefOrder, PaginatedResponse, Order } from '@/models'

export const getFailedOrdersList = (
  page = 1,
  filter?: string
): Promise<PaginatedResponse<RefOrder>> => {
  return Api.req(
    'GET',
    `/api/corporate/v1/reforder?page=${page}&filter[status]=canceled${
      filter ? `&${filter}` : ''
    }`
  )
}

export const getOrdersList = (
  page = 1,
  filter?: string
): Promise<PaginatedResponse<Order>> => {
  return Api.req(
    'GET',
    `/api/corporate/v1/order?page=${page}&filter[has_cash_on_delivery]=All&filter[status]=in_progress${
      filter ? `&${filter}` : ''
    }`
  )
}

export const downloadExcelFromOrders = (page = 1) => {
  return Api.req(
    'GET',
    `/api/corporate/v1/order?page=${page}&filter[has_cash_on_delivery]=All&filter[status]=in_progress&filter[from_date]=undefined&filter[to_date]=undefined&download_as_file=Yes`,
    {},
    true
  )
}

export const deleteOrder = (id: number) => {
  return Api.req('PATCH', `/api/corporate/v1/order/${id}/cancel`)
}

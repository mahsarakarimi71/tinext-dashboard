import * as React from 'react'
import {
  Button,
  CenterLoading,
  ColumnType,
  Heading,
  Icon,
  IndexIndicator,
  MobileTable,
  Modal,
  Pagination,
  Search,
  Table
} from '@/components'
import { Order } from '@/models'
import {
  useDeleteOrder,
  useDownloadExcelFromOrders,
  useGetOrders
} from './hooks'
import { displayToast } from '@/lib'

type State = {
  modalStatus: boolean
  orderId: number | null
}

const OrdersList = () => {
  const [page, setPage] = React.useState(1)
  const [filter, setFilter] = React.useState('')
  const [deleteModal, setDeleteModal] = React.useState<State>({
    modalStatus: false,
    orderId: null
  })

  const {
    isLoading,
    data,
    refetch: refetchOrders,
    isRefetching
  } = useGetOrders(page, filter)
  const { refetch } = useDownloadExcelFromOrders(page, (d) => {
    const file = window.URL.createObjectURL(d)
    window.location.assign(file)
  })

  const { mutate, isLoading: deleteLoading } = useDeleteOrder()

  const columns: ColumnType[] = [
    {
      label: 'تاریخ و زمان',
      accessor: 'persian_date',
      sortable: true
    },
    {
      label: 'تعداد مرسولات',
      accessor: 'postage',
      sortable: true
    },
    {
      label: 'وضعیت',
      accessor: 'fa_status',
      sortable: true
    },
    {
      label: 'بارکد',
      accessor: 'code',
      sortable: true
    },
    {
      label: 'پرینت A4 و label printer',
      accessor: 'custom',
      sortable: false,
      Component: ({ rowData }: { rowData: Order }) => (
        <div className="flex items-center justify-center">
          <button className="ml-2">
            <Icon icon="sabte-sefareh-504" size={18} />
          </button>
          <button>
            <Icon icon="file-copy-2-line" size={18} />
          </button>
        </div>
      )
    },
    {
      label: 'مشاهده لیست مرسوله ها',
      accessor: 'custom_parcels',
      sortable: false,
      Component: ({ rowData }: { rowData: Order }) => (
        <div className="flex items-center justify-center">
          <button>
            <Icon icon="download_data" size={18} />
          </button>
        </div>
      )
    },
    {
      label: 'عملیات',
      accessor: 'operations',
      sortable: false,
      Component: ({ rowData }: { rowData: Order }) => (
        <div className="flex items-center justify-center">
          <button className="ml-2">
            <Icon icon="sabte-sefaresh-1002" size={18} />
          </button>
          <button onClick={() => toggleDeleteModal(rowData.id)}>
            <Icon icon="sabte-sefaresh-1003" size={18} />
          </button>
        </div>
      )
    }
  ]

  const onPageChanged = (pageNumber: number) => {
    setPage(pageNumber)
  }

  const downloadExcel = async () => {
    refetch()
  }

  const onSearch = (value: string) => {
    setFilter(value)
  }

  const toggleDeleteModal = (orderId: number | null = null) => {
    setDeleteModal({
      modalStatus: !deleteModal.modalStatus,
      orderId
    })
  }

  const deleteOrder = () => {
    if (deleteModal.orderId) {
      mutate(deleteModal.orderId, {
        onSuccess: () => {
          toggleDeleteModal()
          refetchOrders()
        },
        onError: (e) => {
          toggleDeleteModal()
          displayToast(e.message, 'error')
        }
      })
    }
  }

  return (
    <div>
      <div className="mb-8">
        <Heading title="سفارش‌های من" />
      </div>
      <div className="mb-10">
        <Search
          searchFields={[
            { label: 'بارکد سفارش', key: 'order_code', type: 'text' },
            {
              label: 'شماره همراه گیرنده',
              key: 'customer_phone',
              type: 'number'
            }
          ]}
          onSearch={onSearch}
        />
      </div>
      {isLoading || isRefetching ? (
        <CenterLoading />
      ) : (
        <>
          <div className="mb-10 flex items-center justify-between">
            <h3 className="text-lg font-bold">{data?.meta.total} مورد</h3>
            <Button onClick={downloadExcel} type="outlined">
              <Button.Icon icon="safreshe-man01" size={20} />
              <Button.Text>دریافت خروجی اکسل</Button.Text>
            </Button>
          </div>
          <Table data={data?.data || []} columns={columns} page={page} />
          <div className="lg:hidden">
            {data?.data.map((item, index) => {
              return (
                <MobileViewItem
                  item={item}
                  key={item.id}
                  index={(page - 1) * 15 + index + 1}
                  deleteOnClick={() => toggleDeleteModal(item.id)}
                />
              )
            })}
          </div>
          <Pagination
            currentPage={page}
            totalCount={data?.meta.total || 0}
            totalPages={data?.meta.total_pages || 0}
            onPageChanged={onPageChanged}
          />
        </>
      )}
      <Modal isOpen={deleteModal.modalStatus}>
        <Modal.Body>
          <Modal.Header title="حذف سفارش" onClose={toggleDeleteModal} />
          <p>آیا از حذف سفارش اطمینان دارید؟</p>
          <div className="mt-12 flex items-center justify-center">
            <Button onClick={() => toggleDeleteModal(null)} type="secondary">
              <Button.Text>خیر</Button.Text>
            </Button>
            <Button onClick={deleteOrder}>
              {deleteLoading ? (
                <Button.Loading />
              ) : (
                <Button.Text>بله</Button.Text>
              )}
            </Button>
          </div>
        </Modal.Body>
      </Modal>
    </div>
  )
}

const MobileViewItem = ({
  item,
  index,
  deleteOnClick
}: {
  item: Order & { persian_date: string }
  index: number
  deleteOnClick: () => void
}) => {
  return (
    <MobileTable>
      <MobileTable.Row justify="between">
        <div className="flex items-center">
          <IndexIndicator index={index} withMargin={false} />
          <p className="mr-2 text-sm text-masterBlack">{item.code}</p>
        </div>
        <p className="text-sm text-masterBlack">{item.persian_date}</p>
      </MobileTable.Row>
      <MobileTable.Row justify="between">
        <p className="text-sm text-masterBlack">
          تعداد مرسولات: {item.postage}
        </p>
      </MobileTable.Row>
      <MobileTable.Row justify="between">
        <p className="text-sm text-masterBlack">وضعیت: {item.fa_status}</p>
      </MobileTable.Row>
      <MobileTable.Row justify="between">
        <button>Label</button>
        <button>مشاهده لیست مرسولات</button>
      </MobileTable.Row>
      <MobileTable.Row justify="between">
        <button>A4</button>
        <div className="flex items-center justify-center">
          <button className="ml-2">
            <Icon icon="sabte-sefaresh-1002" size={18} />
          </button>
          <button onClick={deleteOnClick}>
            <Icon icon="sabte-sefaresh-1003" size={18} />
          </button>
        </div>
      </MobileTable.Row>
    </MobileTable>
  )
}

export default OrdersList

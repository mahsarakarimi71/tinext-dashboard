export default {
  getFailedOrders: 'getFailedOrders',
  getOrdersList: 'getOrdersList',
  downloadExcel: 'downloadExcel'
}

import { useQuery } from '@tanstack/react-query'
import { downloadExcelFromOrders } from '../api'
import queryKeys from './queryKeys'

const useDownloadExcelFromOrders = (page = 1, onSuccess: (d: any) => void) => {
  return useQuery(
    [queryKeys.downloadExcel, page],
    () => downloadExcelFromOrders(page),
    {
      enabled: false,
      onSuccess
    }
  )
}

export default useDownloadExcelFromOrders

import { useMutation } from '@tanstack/react-query'
import { deleteOrder } from '../api'

const useDeleteOrder = () => {
  const data = useMutation<any, Error, number, unknown>((id: number) =>
    deleteOrder(id)
  )
  return data
}

export default useDeleteOrder

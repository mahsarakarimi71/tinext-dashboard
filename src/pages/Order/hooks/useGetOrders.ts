import { useQuery } from '@tanstack/react-query'
import queryKeys from './queryKeys'
import { getOrdersList } from '../api'
import { Order, PaginatedResponse } from '@/models'
import { convertDateToPersian, getTimeFromDate } from '@/lib'

const transform = (orders: PaginatedResponse<Order>) => {
  const data = orders.data.map((order) => {
    return {
      ...order,
      persian_date:
        convertDateToPersian(order.created_at.replace(' ', 'T')) +
        ' ' +
        getTimeFromDate(order.created_at.replace(' ', 'T'))
    }
  })
  return {
    data,
    meta: orders.meta.pagination
  }
}

const useGetOrders = (page: number, filter?: string) => {
  return useQuery(
    [queryKeys.getOrdersList, page, filter],
    () => getOrdersList(page, filter),
    {
      select: transform
    }
  )
}

export default useGetOrders

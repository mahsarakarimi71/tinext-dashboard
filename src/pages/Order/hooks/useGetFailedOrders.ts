import { useQuery } from '@tanstack/react-query'
import queryKeys from './queryKeys'
import { getFailedOrdersList } from '../api'
import { RefOrder, PaginatedResponse, Pagination } from '@/models'
import { convertDateToPersian, currencyFormat } from '@/lib'

export type TransformedFailedOrder = {
  data: RefOrder[] &
    {
      corporate_name: string
      corporate_phone_no: string
      corporate_address: string
      pickup_time: string
      corporate_desc: string
      price_formatted: React.ReactNode
    }[]
  meta: Pagination
}

const transformOrders = (
  orders: PaginatedResponse<RefOrder>
): TransformedFailedOrder => {
  const data = orders.data.map((order) => {
    return {
      ...order,
      corporate_name: order.corporate.name,
      corporate_phone_no: order.corporate.phone_no,
      corporate_address: order.corporate.address,
      cost: order.cost || 0,
      pickup_time: `${convertDateToPersian(
        order.pickup_at.replace(' ', 'T')
      )}\n${order.pickup_timespan}`,
      corporate_desc: order.corporate.description,
      price_formatted: currencyFormat(order.cash_on_delivery || 0)
    }
  })
  return {
    data,
    meta: orders.meta.pagination
  }
}

const useGetFailedOrders = (page = 1, filter?: string) => {
  return useQuery(
    [queryKeys.getFailedOrders, page, filter],
    () => getFailedOrdersList(page, filter),
    {
      select: transformOrders
    }
  )
}

export default useGetFailedOrders

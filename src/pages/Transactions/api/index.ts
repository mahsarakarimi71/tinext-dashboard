import { Api } from '@/lib'

export const getAccountBalance = () => {
  return Api.req('GET', '/api/corporate/v1/balance')
}

import { useUserStore } from '@/hooks'
import { useQuery } from '@tanstack/react-query'
import { getAccountBalance } from '../api'
import queryKeys from './queryKeys'

const useGetBalance = () => {
  return useQuery([queryKeys.getAccountBalance], getAccountBalance, {
    onSuccess: (data) => {
      useUserStore.getState().updateUser(data)
    }
  })
}

export default useGetBalance

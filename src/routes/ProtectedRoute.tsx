import { Navigate, Outlet } from 'react-router-dom'
import { useFetchUser } from '@/pages/Auth/hooks'
import { PageLoading } from '@/components'
import { useGetBalance } from '@/pages/Transactions/hooks'

type Props = {
  children?: JSX.Element
}

const ProtectedRoute = ({ children }: Props) => {
  const { isLoading, data, error } = useFetchUser()
  const { isLoading: balanceLoading } = useGetBalance()
  if (isLoading || balanceLoading) {
    return <PageLoading />
  }
  if (!data || error) {
    return <Navigate to="/login-or-register" replace />
  }

  return children ? children : <Outlet />
}

export default ProtectedRoute

import * as React from 'react'
import { Routes, Route } from 'react-router-dom'
import ProtectedRoute from './ProtectedRoute'
import { Dashboard } from '@/components'
import {
  LoginOrRegister,
  Login,
  VerifyEmail,
  Verification,
  Register,
  Contract,
  Dashboard as DashboardPage,
  MessagesList,
  FailedOrdersList,
  TransactionsList,
  OrdersList,
  TicketsList,
  Profile,
  AddDepot,
  AddUser,
  AddTicket,
  SupportedAreas
} from '@/pages'

const AppRouter = () => (
  <Routes>
    <Route path="/login-or-register" element={<LoginOrRegister />} />
    <Route path="/login" element={<Login />} />
    <Route path="/register">
      <Route path="verify-email" element={<VerifyEmail />} />
      <Route path="verification" element={<Verification />} />
      <Route path="details" element={<Register />} />
      <Route path="contract" element={<Contract />} />
    </Route>
    <Route element={<ProtectedRoute />}>
      <Route path="/" element={<Dashboard />}>
        <Route index element={<DashboardPage />} />
        <Route path="/my-orders" element={<OrdersList />} />
        <Route path="/messages" element={<MessagesList />} />
        <Route path="/failed-deliveries" element={<FailedOrdersList />} />
        <Route path="/transactions" element={<TransactionsList />} />
        <Route path="/support" element={<TicketsList />} />
        <Route path="/support/open-ticket" element={<AddTicket />} />
        <Route path="/supported-areas" element={<SupportedAreas />} />
        <Route path="/profile" element={<Profile />} />
        <Route path="/profile/add-depot" element={<AddDepot />} />
        <Route path="/profile/add-user" element={<AddUser />} />
      </Route>
    </Route>
  </Routes>
)

export default AppRouter

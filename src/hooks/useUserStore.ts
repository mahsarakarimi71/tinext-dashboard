import create from 'zustand'
import { User } from '@/models'

type State = {
  user: User
  setUser: (user: User) => void
  updateUser: (user: Partial<User>) => void
}

const useUserStore = create<State>((set) => ({
  user: {} as User,
  setUser: (user) => set(() => ({ user })),
  updateUser: (d) => set(({ user }) => ({ user: { ...user, ...d } }))
}))

export default useUserStore

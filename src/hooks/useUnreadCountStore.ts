import create from 'zustand'

type State = {
  unreadCount: number
  setUnreadCount: (unreadCount: number) => void
}

const useUnreadCountStore = create<State>((set) => ({
  unreadCount: 0,
  setUnreadCount: (unreadCount) => set(() => ({ unreadCount }))
}))

export default useUnreadCountStore

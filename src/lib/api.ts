import cookieHandler from './cookieHandler'

export default class Api {
  static API_URL =
    process.env.NODE_ENV === 'production'
      ? 'https://backend.tinextco.com'
      : 'http://demo.backend.tinextco.com'

  static async req(
    method: string,
    url: string,
    data?: any,
    respAsBlob = false
  ) {
    const headers = new Headers()
    headers.append('Content-Type', 'application/json')
    headers.append('Accept', 'application/json')
    const token = cookieHandler.getCookie('jwt_token')
    if (token) {
      headers.append('Authorization', `Bearer ${token}`)
    }
    let options: any = {
      method,
      headers
    }
    if (method !== 'GET' && data) {
      options = {
        ...options,
        body: JSON.stringify(data)
      }
    }

    const response = await fetch(`${Api.API_URL}${url}`, options)
    if (!response.ok) {
      const res = await response.json()
      throw new Error(res.message)
      // return Promise.reject(text)
    }
    if (respAsBlob) {
      return response.blob()
    }
    return response.json()
  }
}

import { toast } from 'react-toastify'

export const getDataFromJson = <T>(json: string) => {
  const data = JSON.parse(json)
  return Object.values(data) as T[]
}

export const displayToast = (
  msg: string,
  type: 'error' | 'warn' | 'success'
) => {
  toast[type](msg, {
    position: 'bottom-left',
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: false,
    progress: undefined,
    theme: 'colored',
    bodyStyle: {
      fontFamily: 'IRANSans'
    }
  })
}

export const convertDateToPersian = (date: string) => {
  return new Intl.DateTimeFormat('fa-IR').format(new Date(date))
}

export const getTimeFromDate = (date: string) => {
  const time = new Date(date)
  return time.getHours() + ':' + time.getMinutes()
}

export const currencyFormat = (value: number | string) => {
  return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
}

export const range = (start: number, end: number) => {
  const length = end - start + 1
  /*
  	Create an array of certain length and set the elements within it from
    start value to end value.
  */
  return Array.from({ length }, (_, idx) => idx + start)
}

export const serialize = (obj: Record<any, any>, prefix?: string): string => {
  const str = []
  let p
  for (p in obj) {
    // eslint-disable-next-line no-prototype-builtins
    if (obj.hasOwnProperty(p)) {
      const k = prefix ? prefix + '[' + p + ']' : p,
        v = obj[p]
      str.push(
        v !== null && typeof v === 'object'
          ? serialize(v, k)
          : encodeURIComponent(k) + '=' + encodeURIComponent(v)
      )
    }
  }
  return str.join('&')
}

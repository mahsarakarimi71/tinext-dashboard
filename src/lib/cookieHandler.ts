import { Cookies } from 'react-cookie'

class CookieHandler {
  cookies: Cookies

  constructor() {
    this.cookies = new Cookies()
  }

  setCookie(name: string, value: string) {
    this.cookies.set(name, value)
  }

  removeCookie(name: string) {
    this.cookies.remove(name)
  }

  getCookie(name: string) {
    return this.cookies.get(name)
  }
}

const cookieHandler = new CookieHandler()
export default cookieHandler

import * as React from 'react'

type Props = React.HTMLProps<HTMLParagraphElement>

const ErrorCaption = ({ children }: Props) => (
  <p className="absolute top-[100%] right-3 mt-1 text-[10px] text-tRed">
    {children}
  </p>
)

export default ErrorCaption

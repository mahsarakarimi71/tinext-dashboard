import Spinner from '../Spinner/Spinner'

const CenterLoading = () => (
  <div className="grid min-h-[200px] w-full place-content-center place-items-center">
    <Spinner />
  </div>
)

export default CenterLoading

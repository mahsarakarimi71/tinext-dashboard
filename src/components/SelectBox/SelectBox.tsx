import * as React from 'react'

type Props = {
  options: Option[]
  onSelect: (value: string) => void
  placeholder?: string
}

type Option = {
  value: string
  label: string
}

const SelectBox = ({ options, onSelect, placeholder }: Props) => {
  const [isToggled, setIsToggled] = React.useState(false)
  const [selectedItem, setSelectedItem] = React.useState<Option | null>(null)

  const toggleDropDown = () => {
    setIsToggled((prev) => !prev)
  }

  const onItemSelect = (item: Option) => {
    setSelectedItem(item)
    onSelect(item.value)
    setIsToggled(false)
  }

  return (
    <div className="relative">
      <div className="w-full rounded-full border border-gray-100 bg-white p-4 text-sm text-masterBlack">
        <button
          onClick={toggleDropDown}
          className="flex h-full w-full items-center justify-between"
        >
          <p>{selectedItem ? selectedItem.label : placeholder}</p>
          <span>⬇️</span>
        </button>
      </div>
      {isToggled && (
        <div className="absolute inset-x-0 top-0 rounded-2xl border border-gray-100 bg-white p-4">
          {options.map((item, index) => (
            <div className="flex items-center justify-between" key={index}>
              <button
                onClick={() => onItemSelect(item)}
                className={`mb-5 block text-sm ${
                  selectedItem?.value === item.value && 'text-primary-500'
                }`}
              >
                {item.label}
              </button>
              {index === 0 && <button onClick={toggleDropDown}>⬆️</button>}
            </div>
          ))}
        </div>
      )}
    </div>
  )
}

export default SelectBox

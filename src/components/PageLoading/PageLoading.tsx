import Logo from '../Logo/Logo'
import Spinner from '../Spinner/Spinner'

const PageLoading = () => (
  <div className="flex h-screen w-screen flex-col items-center justify-center">
    <Logo width="300px" height="300px" />
    <Spinner />
  </div>
)

export default PageLoading

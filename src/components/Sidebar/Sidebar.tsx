import * as React from 'react'
import Button from '../Button/Button'
import Divider from '../Divider/Divider'
import Logo from '../Logo/Logo'
import SIDEBAR_CONTENT from '@/data/sidebarContent.json'
import { getDataFromJson } from '@/lib'
import SidebarBtn from './SidebarBtn'
import { SidebarData } from '@/types'

const SIDEBAR_DATA = getDataFromJson<SidebarData>(
  JSON.stringify(SIDEBAR_CONTENT)
)

const Sidebar = () => {
  const [isCollapsed, setIsCollapsed] = React.useState(false)
  const collapsedClass = isCollapsed ? 'w-[150px]' : 'w-[220px] xl:w-[330px]'

  const toggleCollapsed = () => {
    setIsCollapsed(!isCollapsed)
  }

  return (
    <div
      className={`ml-6 hidden min-h-screen ${collapsedClass} overflow-auto rounded-l-3xl bg-white p-2 transition-all duration-1000 ease-in-out lg:block xl:p-6`}
    >
      <div className="mb-4 flex items-center justify-center">
        <Logo />
      </div>
      <Divider />
      <ul className="flex flex-col px-3">
        {SIDEBAR_DATA.map((item) => (
          <SidebarBtn
            text={item.text}
            key={item.id}
            to={item.link}
            isCollapsed={isCollapsed}
            iconName={item.icon}
          />
        ))}
        <li className="self-end">
          <Button.Round onClick={toggleCollapsed} type="secondary">
            <Button.Icon
              icon="Tinext-Panel14"
              style={isCollapsed ? { transform: 'rotate(180deg)' } : {}}
              size={18}
            />
            {/* <Button.Text>{isCollapsed ? '>' : '<'}</Button.Text> */}
          </Button.Round>
        </li>
      </ul>
    </div>
  )
}

export default Sidebar

import { NavLink } from 'react-router-dom'
import { Icon } from '..'

type SidebarBtnProps = {
  isCollapsed: boolean
  text: string
  className?: string
  iconName: string
} & React.ComponentProps<typeof NavLink>

const SidebarBtn = ({
  isCollapsed,
  text,
  className = '',
  iconName,
  ...restProps
}: SidebarBtnProps) => {
  return (
    <li className={`my-3 p-2 ${className}`}>
      <NavLink
        className={({ isActive }) =>
          isActive && !isCollapsed
            ? 'bg-primary-100 text-primary-400 block w-full lg:w-4/5 p-6 rounded-full relative lg:-mr-4 hover:text-primary-500 -my-4'
            : 'hover:text-primary-400'
        }
        {...restProps}
      >
        <div
          className={`relative flex items-center ${
            isCollapsed ? 'justify-center' : 'justify-start'
          }`}
        >
          {isCollapsed && (
            <div className="absolute inset-x-0 -top-2 right-1 z-0 h-16 w-16 rounded-full bg-primary-100" />
          )}
          <NavLink
            className={({ isActive }) =>
              isActive
                ? 'w-12 h-12 rounded-full bg-white flex justify-center items-center ml-2 text-primary-500 z-20'
                : 'w-12 h-12 rounded-full bg-tGray flex justify-center items-center ml-2 z-20'
            }
            {...restProps}
          >
            <Icon icon={iconName} size={18} />
          </NavLink>
          <span className={`${isCollapsed ? 'hidden' : ''} animate-fade-in`}>
            {text}
          </span>
        </div>
      </NavLink>
    </li>
  )
}

export default SidebarBtn

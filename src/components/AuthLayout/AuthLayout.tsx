import { GeneralProps } from '@/types'
import { Logo, Heading, Icon } from '@/components'
import { useNavigate } from 'react-router-dom'

type Props = {
  title: string
  isFirstStep?: boolean
  hideBack?: boolean
} & GeneralProps

const AuthLayout = ({ title, isFirstStep, hideBack, children }: Props) => {
  const navigate = useNavigate()

  const goBack = () => {
    navigate(-1)
  }

  return (
    <main className="grid min-h-screen w-screen grid-cols-1 place-content-center place-items-center bg-tGray p-2 md:mt-0 lg:p-0">
      <section className="m-auto mt-8 min-h-[450px] w-full rounded-2xl bg-white p-4 py-12 md:w-2/3 lg:w-[800px]">
        <div className="flex flex-col items-center justify-center">
          <div className="mb-12">
            <Logo width="150" height="65" />
          </div>
          <Heading title={title} />
        </div>
        {children}
      </section>
      {!hideBack && (
        <section className="my-8 flex justify-center">
          <div className="flex h-14 w-[160px] items-center justify-center rounded-full bg-white text-center text-masterBlack md:my-0">
            {isFirstStep ? (
              <a href="https://google.com" rel="noreferrer">
                بازگشت به سایت
              </a>
            ) : (
              <button className="text-lg" onClick={goBack}>
                <Icon icon="login02" color="black" size={19} />
                بازگشت
              </button>
            )}
          </div>
        </section>
      )}
    </main>
  )
}

export default AuthLayout

import HeadingBullet from './HeadingBullet'

type Props = {
  title: string
}

const Heading = ({ title }: Props) => (
  <div className="flex items-center">
    <HeadingBullet />
    <h2 className="text-lg font-bold xl:text-xl">{title}</h2>
  </div>
)

export default Heading

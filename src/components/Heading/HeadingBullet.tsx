const HeadingBullet = () => (
  <div className="ml-3 h-4 w-4 rounded-r-full bg-primary-400" />
)

export default HeadingBullet

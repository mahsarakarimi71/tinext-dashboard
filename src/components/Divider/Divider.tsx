const Divider = () => <div className="mx-6 h-[2px] bg-masterBlack opacity-10" />

export default Divider

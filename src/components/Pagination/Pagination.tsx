import { range } from '@/lib'
import * as React from 'react'

type Props = {
  currentPage: number
  totalPages: number
  totalCount: number
  onPageChanged: (page: number) => void
}

type PaginationItemProps = {
  text: string
  onClick: () => void
  isActive?: boolean
  disabled?: boolean
}

const DOTS = '...'

const Pagination = ({
  currentPage,
  totalPages,
  onPageChanged,
  totalCount
}: Props) => {
  const paginationRange = usePagination({
    totalCount,
    currentPage,
    pageSize: 15
  })

  const lastPage = paginationRange
    ? paginationRange[paginationRange?.length - 1]
    : totalPages

  return (
    <ul className="flex items-center justify-center md:justify-end">
      <PaginationItem
        text="صفحه قبلی"
        onClick={() => onPageChanged(currentPage - 1)}
        disabled={currentPage === 1}
      />
      <div className="hidden md:flex md:items-center md:justify-end">
        {paginationRange?.map((pageNumber, index) => {
          if (pageNumber === DOTS) {
            return <li key={`dot-${index}`}>&#8230;</li>
          }
          const num = parseInt(pageNumber.toString(), 10)
          return (
            <PaginationItem
              onClick={() => onPageChanged(num)}
              text={pageNumber.toString()}
              key={pageNumber}
              isActive={pageNumber === currentPage}
            />
          )
        })}
      </div>
      <div className="md:hidden">
        <PaginationItem
          onClick={() => onPageChanged(currentPage)}
          isActive
          text={currentPage.toString()}
        />
      </div>
      <PaginationItem
        text="صفحه بعدی"
        onClick={() => onPageChanged(currentPage + 1)}
        disabled={currentPage === lastPage}
      />
      <li className="cursor-pointer text-[12px] font-bold"></li>
    </ul>
  )
}

const PaginationItem = ({
  text,
  onClick,
  isActive,
  disabled
}: PaginationItemProps) => {
  const onPageChange = () => {
    if (!isActive && !disabled) {
      onClick()
    }
  }

  const classes = `cursor-pointer text-[12px] font-bold mx-1 ${
    isActive ? 'bg-primary-100 text-primary-500' : ''
  } px-4 py-3 rounded-md hover:text-primary-500 ${
    disabled && 'opacity-50 cursor-not-allowed'
  }`

  return (
    <li onClick={onPageChange} className={classes}>
      {text}
    </li>
  )
}

type PaginationProps = {
  totalCount: number
  pageSize: number
  siblingCount?: number
  currentPage: number
}

const usePagination = ({
  totalCount,
  pageSize,
  siblingCount = 1,
  currentPage
}: PaginationProps) => {
  const paginationRange = React.useMemo(() => {
    const totalPageCount = Math.ceil(totalCount / pageSize)
    const totalPageNumbers = siblingCount + 5

    if (totalPageNumbers >= totalPageCount) {
      return range(1, totalPageCount)
    }

    const leftSiblingIndex = Math.max(currentPage - siblingCount, 1)
    const rightSiblingIndex = Math.min(
      currentPage + siblingCount,
      totalPageCount
    )

    const shouldShowLeftDots = leftSiblingIndex > 2
    const shouldShowRightDots = rightSiblingIndex < totalPageCount - 2

    const firstPageIndex = 1
    const lastPageIndex = totalPageCount

    if (!shouldShowLeftDots && shouldShowRightDots) {
      const leftItemCount = 3 + 2 * siblingCount
      const leftRange = range(1, leftItemCount)

      return [...leftRange, DOTS, totalPageCount]
    }

    if (shouldShowLeftDots && !shouldShowRightDots) {
      const rightItemCount = 3 + 2 * siblingCount
      const rightRange = range(
        totalPageCount - rightItemCount + 1,
        totalPageCount
      )
      return [firstPageIndex, DOTS, ...rightRange]
    }

    if (shouldShowLeftDots && shouldShowRightDots) {
      const middleRange = range(leftSiblingIndex, rightSiblingIndex)
      return [firstPageIndex, DOTS, ...middleRange, DOTS, lastPageIndex]
    }
  }, [totalCount, pageSize, siblingCount, currentPage])

  return paginationRange
}

export default Pagination

import { GeneralProps } from '@/types'
import { NavLink } from 'react-router-dom'
import Icon from '../Icon/Icon'
import Spinner from '../Spinner/Spinner'

type ButtonProps = React.HTMLProps<HTMLButtonElement> & {
  type?: 'primary' | 'outlined' | 'secondary' | 'secondary-no-hover'
  fullWidth?: boolean
  btnType?: 'button' | 'submit' | 'reset'
} & GeneralProps

type RoundedBtnProps = React.HTMLProps<HTMLButtonElement> & {
  type?: 'primary' | 'outlined' | 'secondary' | 'secondary-no-hover'
  btnSize?: string
} & GeneralProps

type NavBtnProps = React.ComponentProps<typeof NavLink> & {
  type?: 'primary' | 'outlined' | 'secondary' | 'secondary-no-hover'
  rounded?: boolean
}

const primaryButtonClass = 'bg-primary-500 text-white hover:bg-primary-700'
const outlinedButtonClass =
  'bg-transparent border-primary-500 text-primary-500 border-2 hover:border-primary-700 hover:text-primary-700'

const secondaryButtonClass =
  'bg-tGray text-masterBlack hover:bg-primary-500 hover:text-white'

const secondaryNoHoverButtonClass = 'bg-tGray text-masterBlack'

const getButtonClass = (type: ButtonProps['type']) => {
  switch (type) {
    case 'primary':
      return primaryButtonClass
    case 'outlined':
      return outlinedButtonClass
    case 'secondary':
      return secondaryButtonClass
    case 'secondary-no-hover':
      return secondaryNoHoverButtonClass
    default:
      return primaryButtonClass
  }
}

const Button = ({
  type = 'primary',
  fullWidth,
  children,
  btnType = 'button',
  ...restProps
}: ButtonProps) => {
  const btnClass = getButtonClass(type)
  const width = fullWidth ? 'w-full' : 'min-w-[110px]'
  return (
    <button
      className={`mx-1 min-h-[46px] ${width} rounded-full py-3 px-6 text-sm disabled:opacity-80 xl:text-base ${btnClass}`}
      type={btnType}
      {...restProps}
    >
      {children}
    </button>
  )
}

const RoundButton = ({
  type = 'primary',
  children,
  btnSize = '50px',
  ...restProps
}: RoundedBtnProps) => {
  const btnClass = getButtonClass(type)
  const btnDimensions = `w-[40px] h-[40px] xl:w-[${btnSize}] xl:h-[${btnSize}]`
  return (
    <button
      className={`mx-1 ${btnDimensions} rounded-full text-sm 2xl:text-base ${btnClass}`}
      {...restProps}
    >
      {children}
    </button>
  )
}

const NavButton = ({
  children,
  type = 'primary',
  rounded,
  ...restProps
}: NavBtnProps) => {
  const btnClass = getButtonClass(type)
  const roundedClass = rounded ? 'p-3' : 'py-3 px-4 xl:py-3 xl:px-6'
  return (
    <NavLink
      className={`mx-1 inline-block rounded-full text-[11px] xl:text-sm 2xl:text-base ${roundedClass} ${btnClass}`}
      {...restProps}
    >
      {children}
    </NavLink>
  )
}

const ButtonText = ({ children, ...restProps }: GeneralProps) => (
  <span className="font-irSans text-sm 2xl:text-base" {...restProps}>
    {children}
  </span>
)

type BtnIconProps = React.ComponentProps<typeof Icon>

const ButtonIcon = ({ ...restProps }: BtnIconProps) => <Icon {...restProps} />

const Loading = () => (
  <div className="grid place-content-center">
    <Spinner />
  </div>
)

Button.Text = ButtonText
Button.Icon = ButtonIcon
Button.NavBtn = NavButton
Button.Round = RoundButton
Button.Loading = Loading

export default Button

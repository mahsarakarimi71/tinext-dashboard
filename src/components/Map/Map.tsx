import { GeneralProps } from '@/types'
import * as React from 'react'
import Map, { MapRef } from 'react-map-gl'

type Props = {
  zoom?: number
  withMarker?: boolean
} & GeneralProps

type Ref = MapRef

// eslint-disable-next-line react/display-name
const MapView = React.forwardRef<Ref, Props>(
  ({ zoom = 14, withMarker = true, children }, ref) => {
    return (
      <Map
        initialViewState={{
          latitude: 35.7448416,
          longitude: 51.3731325,
          zoom
        }}
        mapboxAccessToken="sk.eyJ1IjoidGVocmFubWFwYm94MSIsImEiOiJjbDhpbWh6dTkwa2RqM25sY3ZrZDN1dTV3In0.q_9wAoFU-RNmxkIe7SGxCg"
        mapStyle="https://api.parsimap.ir/styles/parsimap-streets-v11?key=p118c208bb511542dcbcc4032e12208d3a1f2dec8a"
        style={{ height: 400, position: 'relative' }}
        id="mapA"
        ref={ref}
      >
        {withMarker && (
          <div className="absolute top-[33%] left-[43%]">
            <img
              src="/src/public/marker.png"
              alt="marker"
              className="h-[82px] w-[82px]"
            />
          </div>
        )}
        {children}
      </Map>
    )
  }
)

export default MapView

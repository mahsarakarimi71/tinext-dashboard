import Heading from '../Heading/Heading'
import Icon from '../Icon/Icon'

type Props = {
  isOpen: boolean
  children: React.ReactNode
}

type CloseButtonProps = {
  onClick: () => void
}

type ModalHeaderProps = {
  title: string
  onClose?: () => void
}

const Modal = ({ isOpen, children }: Props) => {
  // if (!isOpen) {
  //   return null
  // }

  const classes = `fixed inset-0 z-30 flex items-end justify-end bg-[#0000004f] md:items-center md:justify-center ${
    !isOpen && 'hidden'
  }`

  return <div className={classes}>{children}</div>
}

const ModalBody = ({ children }: { children: React.ReactNode }) => (
  <div className="relative min-h-[300px] w-full rounded-t-2xl bg-white p-8 md:w-3/4 md:rounded-2xl 2xl:w-1/2">
    {children}
  </div>
)

const ModalHeader = ({ title, onClose }: ModalHeaderProps) => {
  return (
    <div className="mb-8 flex items-center justify-between">
      <Heading title={title} />
      {onClose && <CloseButton onClick={onClose} />}
    </div>
  )
}

const CloseButton = ({ onClick }: CloseButtonProps) => (
  <button className="h-10 w-10 rounded-full bg-tGray" onClick={onClick}>
    <Icon icon="sabte-sefaresh-601" size={25} />
  </button>
)

Modal.Body = ModalBody
Modal.Header = ModalHeader
Modal.CloseButton = CloseButton

export default Modal

import { useGetUnreadCount } from '@/pages/Messages/hooks'
import { Outlet } from 'react-router-dom'
import Header from '../Header/Header'
import Sidebar from '../Sidebar/Sidebar'

const Dashboard = () => {
  useGetUnreadCount()
  return (
    <div className="bg-tGray pl-4">
      <div className="flex">
        <div className="h-full">
          <Sidebar />
        </div>
        <div className="flex-1">
          <Header />
          <main className="my-4 w-screen rounded-2xl bg-white p-10 lg:w-auto">
            <Outlet />
          </main>
        </div>
      </div>
    </div>
  )
}

export default Dashboard

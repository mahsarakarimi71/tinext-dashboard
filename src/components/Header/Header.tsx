import * as React from 'react'
import { NavLink, useNavigate } from 'react-router-dom'
import Button from '../Button/Button'
import Divider from '../Divider/Divider'
import Logo from '../Logo/Logo'
import SIDEBAR_CONTENT from '@/data/sidebarContent.json'
import { cookieHandler, currencyFormat, getDataFromJson } from '@/lib'
import SidebarBtn from '../Sidebar/SidebarBtn'
import { SidebarData } from '@/types'
import { useLogout } from '@/pages/Auth/hooks'
import { useUnreadCountStore, useUserStore } from '@/hooks'
import { Icon } from '..'

const SIDEBAR_DATA = getDataFromJson<SidebarData>(
  JSON.stringify(SIDEBAR_CONTENT)
)

const Header = () => {
  const { unreadCount } = useUnreadCountStore()
  const { user } = useUserStore()

  return (
    <>
      <header className="hidden rounded-b-3xl bg-white p-2 lg:block xl:p-6">
        <nav className="flex items-center justify-between">
          <ul className="flex items-center">
            <li>
              <Button.NavBtn to="/new-order">
                <Icon icon="Tinext-Panel06" color="white" size={20} />
                ثبت سفارش جدید
              </Button.NavBtn>
            </li>
            <li>
              <Button.NavBtn type="secondary" to="/credit">
                <Icon icon="sabte-sefaresh-1102" color="#03053D" size={22} />
                اعتبار حساب:{' '}
                <b className="text-lg font-bold">
                  {currencyFormat(user.balance || 0)}
                </b>{' '}
                <small>تومان</small>
              </Button.NavBtn>
            </li>
            <li>
              <Button.NavBtn type="secondary" to="/cod-credit">
                <Icon icon="hand-coin-line" color="#03053D" size={22} />
                اعتبار پرداخت در محل:{' '}
                <b className="text-lg font-bold">
                  {currencyFormat(user.cash_on_delivery_balance || 0)}
                </b>{' '}
                <small>تومان</small>
              </Button.NavBtn>
            </li>
          </ul>

          <ul className="flex items-center">
            <li className="relative">
              <Button.Round type="secondary">
                <NavLink to="/messages">
                  <Icon icon="Tinext-Panel01" size={20} color="black" />
                </NavLink>
              </Button.Round>
              {unreadCount > 0 && (
                <div className="absolute -top-2 -right-3 grid h-8 w-8 place-content-center rounded-full bg-tRed text-white xl:-top-4">
                  <span className="text-xl">{unreadCount}</span>
                </div>
              )}
            </li>
            <UserProfile />
          </ul>
        </nav>
      </header>
      <MobileNav />
    </>
  )
}

const MobileNav = () => {
  const [showingNav, setNavShowing] = React.useState(false)

  const toggleCollapsed = () => {
    setNavShowing(!showingNav)
  }

  const parentRounded = showingNav ? '' : 'rounded-b-3xl'

  return (
    <>
      <div
        className={`flex w-screen items-center justify-between ${parentRounded} bg-white p-6 lg:hidden`}
      >
        <div className="flex items-center">
          <div className="ml-4">
            <Button onClick={toggleCollapsed}>
              {/* <Button.Text>-</Button.Text> */}
              <Button.Icon icon="menu-line" size={30} />
            </Button>
          </div>
          <Logo />
        </div>
        <ul className="relative flex flex-1 justify-end">
          <UserProfile />
        </ul>
      </div>
      <div className="relative">
        <div
          className={`${
            !showingNav ? 'translate-x-[150%]' : 'translate-x-0'
          } fixed z-20 h-full w-screen origin-top overflow-auto bg-white pb-36 transition-transform duration-700 ease-linear`}
        >
          <Divider />
          <MobileNavList onClick={toggleCollapsed} />
        </div>
      </div>
    </>
  )
}

type MobileNavListProps = {
  onClick: () => void
}

const MobileNavList = ({ onClick }: MobileNavListProps) => (
  <ul className="flex flex-col items-center justify-center">
    {SIDEBAR_DATA.map((item) => (
      <SidebarBtn
        text={item.text}
        key={item.id}
        to={item.link}
        isCollapsed={false}
        onClick={onClick}
        iconName={item.icon}
        className="w-full px-28"
        // margin="small"
      />
    ))}
  </ul>
)

const UserProfile = () => {
  const [isCollapsed, setIsCollapsed] = React.useState(false)

  const navigate = useNavigate()
  const { mutate } = useLogout()

  const { user } = useUserStore()

  const toggleCollapsed = () => {
    setIsCollapsed(!isCollapsed)
  }

  const collapsedClass = `${!isCollapsed ? 'hidden' : 'block'}`

  const logout = () => {
    mutate(undefined, {
      onSuccess: () => {
        cookieHandler.removeCookie('jwt_token')
        navigate('/login-or-register', { replace: true })
      }
    })
  }

  return (
    <li className="lg:relative">
      <div className="hidden lg:block">
        <Button onClick={toggleCollapsed} type="secondary-no-hover">
          <div className="flex items-center justify-between">
            <p className="ml-3">
              {user.full_name ? user.full_name : 'No Name'}
            </p>
            <Icon
              icon="Tinext-Panel14"
              style={{ transform: 'rotate(90deg)' }}
              size={11}
            />
          </div>
        </Button>
      </div>
      <div className="block lg:hidden">
        <Button onClick={toggleCollapsed} type="secondary-no-hover">
          <div className="flex items-center justify-between">
            <Icon icon="Tinext-Panel16" size={20} />
            <Icon
              icon="Tinext-Panel14"
              style={{ transform: 'rotate(90deg)' }}
              size={11}
            />
          </div>
        </Button>
      </div>
      <div>
        <div
          className={`${collapsedClass} absolute -top-2 left-0 z-30 w-full rounded-xl bg-tGray shadow-md md:w-5/12 lg:w-full`}
        >
          <button onClick={toggleCollapsed}>
            <div className="flex items-center justify-between">
              <p className="mx-6 my-4 text-sm lg:text-base">
                {user.full_name ? user.full_name : 'No Name'}
              </p>
              <Icon
                icon="Tinext-Panel14"
                style={{ transform: 'rotate(-90deg)' }}
                size={11}
              />
            </div>
          </button>
          <Divider />
          <button onClick={logout}>
            <p className="mx-6 my-4 text-sm lg:text-base">خروج</p>
          </button>
        </div>
      </div>
    </li>
  )
}

export default Header

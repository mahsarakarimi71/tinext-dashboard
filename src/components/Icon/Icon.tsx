import IcoMoon, { IconProps } from 'react-icomoon'
import iconSet from '../../selection.json'

const Icon = (props: IconProps) => {
  return (
    <IcoMoon
      iconSet={iconSet}
      size={props.size || 14}
      {...props}
      style={{ marginRight: '6px', marginLeft: '6px', ...props.style }}
    />
  )
}

export default Icon

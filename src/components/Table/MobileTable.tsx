import { GeneralProps } from '@/types'

type RowProps = GeneralProps & {
  justify: 'start' | 'center' | 'end' | 'between'
}

const MobileTable = ({ children, ...restProps }: GeneralProps) => (
  <div
    className="my-2 grid gap-2 rounded-md p-4"
    style={{ backgroundColor: 'rgba(247, 247, 247, 0.7)' }}
    {...restProps}
  >
    {children}
  </div>
)

const Row = ({ children, justify, ...restProps }: RowProps) => {
  const justifyClass = `justify-${justify}`
  return (
    <div className={`my-1 flex items-center ${justifyClass}`} {...restProps}>
      {children}
    </div>
  )
}

const Caption = ({ children, ...restProps }: GeneralProps) => (
  <p {...restProps}>{children}</p>
)

MobileTable.Row = Row
MobileTable.Caption = Caption

export default MobileTable

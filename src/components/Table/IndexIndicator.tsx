type Props = {
  index: number
  withMargin?: boolean
}

const IndexIndicator = ({ index, withMargin = true }: Props) => (
  <p
    className={`${
      withMargin && 'ml-6'
    } flex h-7 w-7 items-center justify-center self-start rounded-full text-sm text-masterBlack`}
    style={{ backgroundColor: 'rgba(58, 58, 58, 0.07)' }}
  >
    {index}
  </p>
)

export default IndexIndicator

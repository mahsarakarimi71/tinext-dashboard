import * as React from 'react'
import { Icon } from '..'
import IndexIndicator from './IndexIndicator'

export type ColumnType = {
  label: string
  accessor: string
  sortable: boolean
  sortbyOrder?: 'asc' | 'desc'
  Component?: any
}

export type TableButtonsType = {
  label: string
  onClick: (id: number) => void
}

type Props = {
  data: any[]
  columns: ColumnType[]
  leftElement?: React.ReactNode
  buttons?: TableButtonsType[]
  page: number
}

const Table = ({ data, columns, buttons, page }: Props) => {
  const [tableData, handleSorting] = useSortableTable(data, columns, page)
  return (
    <>
      <table className="hidden w-full lg:table">
        <TableHead {...{ columns, handleSorting }} />
        <TableBody {...{ columns, tableData, buttons, page }} />
      </table>
    </>
  )
}

type TableHeadProps = {
  columns: ColumnType[]
  handleSorting: (sortField: string, sortOrder: 'asc' | 'desc') => void
}

const TableHead = ({ columns, handleSorting }: TableHeadProps) => {
  const [sortField, setSortField] = React.useState('')
  const [order, setOrder] = React.useState('asc')

  const handleSortingChange = (accessor: string, sortOrder: 'asc' | 'desc') => {
    // const sortOrder = accessor === sortField && order === 'asc' ? 'desc' : 'asc'
    setSortField(accessor)
    setOrder(sortOrder)
    handleSorting(accessor, sortOrder)
  }

  const onClick = (accessor: string, sortOrder: 'asc' | 'desc') => {
    handleSortingChange(accessor, sortOrder)
  }

  return (
    <thead className="border-b-[10px] border-b-white">
      <tr>
        {columns.map(({ label, accessor, sortable }) => {
          return (
            <th
              key={accessor}
              // onClick={() => onClick(sortable, accessor)}
              className="text-[12px] font-normal text-masterBlack"
            >
              <div className="flex items-center justify-center">
                <p className="ml-2">{label}</p>
                {sortable && (
                  <div className="flex flex-col">
                    <Icon
                      icon="Tinext-Panel14"
                      style={{
                        transform: 'rotate(-90deg)',
                        color:
                          sortField === accessor && order === 'asc'
                            ? '#56B94A'
                            : '#3A3A3A',
                        cursor: 'pointer'
                      }}
                      onClick={() => onClick(accessor, 'asc')}
                    />
                    <Icon
                      icon="Tinext-Panel14"
                      style={{
                        transform: 'rotate(90deg)',
                        color:
                          sortField === accessor && order === 'desc'
                            ? '#56B94A'
                            : '#3A3A3A',
                        cursor: 'pointer'
                      }}
                      onClick={() => onClick(accessor, 'desc')}
                    />
                  </div>
                )}
              </div>
            </th>
          )
        })}
      </tr>
    </thead>
  )
}

const TableBody = ({
  tableData,
  columns,
  buttons,
  page
}: {
  tableData: any[]
  columns: ColumnType[]
  buttons?: TableButtonsType[]
  page: number
}) => {
  return (
    <tbody className="w-full">
      {tableData.map((data, index) => {
        return (
          <tr
            className="border-b-[8px] border-b-white bg-tGray text-center"
            // @ts-ignore
            style={{ '--tw-bg-opacity': 0.7 }}
            key={data.id}
          >
            {columns.map(({ accessor, Component }, i) => {
              const tData = data[accessor] ? data[accessor] : '——'
              return (
                <td
                  className="relative p-4"
                  key={accessor}
                  style={{ whiteSpace: 'pre-wrap' }}
                >
                  <div className="flex items-center justify-center">
                    {i === 0 && (
                      <IndexIndicator index={(page - 1) * 15 + index + 1} />
                    )}
                    {Component ? (
                      <Component rowData={data} />
                    ) : (
                      <p className="flex-1">{tData}</p>
                    )}
                  </div>
                </td>
              )
            })}
          </tr>
        )
      })}
    </tbody>
  )
}

function getDefaultSorting(defaultTableData: any[], columns: ColumnType[]) {
  const sorted = [...defaultTableData].sort((a, b) => {
    const filterColumn = columns.filter((column) => column.sortbyOrder)

    // Merge all array objects into single object and extract accessor and sortbyOrder keys
    const { accessor = 'id', sortbyOrder = 'asc' } = Object.assign(
      {},
      ...filterColumn
    )

    if (a[accessor] === null) return 1
    if (b[accessor] === null) return -1
    if (a[accessor] === null && b[accessor] === null) return 0

    const ascending = a[accessor]
      .toString()
      .localeCompare(b[accessor].toString(), 'fa', {
        numeric: true
      })

    return sortbyOrder === 'asc' ? ascending : -ascending
  })
  return sorted
}

export const useSortableTable = (
  data: any[],
  columns: ColumnType[],
  page: number
): any[] => {
  const defaultData = getDefaultSorting(data, columns)
  const [tableData, setTableData] = React.useState(defaultData)

  React.useEffect(() => {
    setTableData(defaultData)
  }, [page, defaultData.length])

  const handleSorting = (sortField: string, sortOrder: 'asc' | 'desc') => {
    if (sortField) {
      const sorted = [...tableData].sort((a, b) => {
        if (a[sortField] === null) return 1
        if (b[sortField] === null) return -1
        if (a[sortField] === null && b[sortField] === null) return 0
        return (
          a[sortField].toString().localeCompare(b[sortField].toString(), 'fa', {
            numeric: true
          }) * (sortOrder === 'asc' ? 1 : -1)
        )
      })
      setTableData(sorted)
    }
  }
  return [tableData, handleSorting]
}

export default Table

import * as React from 'react'
import { Button, Input } from '@/components'
import { serialize } from '@/lib'
import Modal from '../Modal/Modal'

type SearchField = {
  label: string
  type?: string
  key: string
}

type Props = {
  searchFields: SearchField[]
  onSearch: (value: string) => void
}

type SearchFieldsProps = {
  searchFields: SearchField[]
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void
}

const Search = ({ searchFields, onSearch }: Props) => {
  const [data, setData] = React.useState({})
  const [modalStatus, setModalStatus] = React.useState(false)

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target
    setData({ ...data, [`filter[${name}]`]: value })
  }

  const setSearch = () => {
    const queryParam = serialize(data)
    onSearch(queryParam)
    if (modalStatus) {
      toggleModalStatus()
    }
  }

  const toggleModalStatus = () => {
    setModalStatus((prev) => !prev)
  }

  return (
    <>
      <div
        className="hidden items-center justify-between rounded-md bg-tGray p-6 md:flex"
        style={{ border: '1px solid rgba(58, 58, 58, 0.1)' }}
      >
        <SearchFields searchFields={searchFields} onChange={onChange} />
        <div>
          <Button onClick={setSearch} type="outlined">
            <Button.Text>جستجو</Button.Text>
            <Button.Icon icon="Tinext-Panel15" size={18} />
          </Button>
        </div>
      </div>
      <button
        className="w-full rounded-md bg-tGray p-6 text-right md:hidden"
        style={{ border: '1px solid rgba(58, 58, 58, 0.1)' }}
        onClick={toggleModalStatus}
      >
        <span>جستجو</span>
      </button>
      <Modal isOpen={modalStatus}>
        <Modal.Body>
          <Modal.Header title="جستجو" onClose={toggleModalStatus} />
          <SearchFields searchFields={searchFields} onChange={onChange} />
          <div className="mt-4 w-full">
            <Button fullWidth onClick={setSearch}>
              <Button.Text>جستجو</Button.Text>
            </Button>
          </div>
        </Modal.Body>
      </Modal>
    </>
  )
}

const SearchFields = ({ searchFields, onChange }: SearchFieldsProps) => {
  return (
    <div className="flex flex-col items-center justify-start md:flex-row">
      {searchFields.map((field, index) => {
        return (
          <div key={index} className="m-4 w-full md:my-0 md:min-w-[200px]">
            <Input.Label
              text={field.label}
              type={field.type || 'text'}
              model="normal"
            />
            <Input.Editor
              model="normal"
              placeholder={field.label}
              onChange={onChange}
              name={field.key}
              //   value={val}
            />
          </div>
        )
      })}
    </div>
  )
}

export default Search

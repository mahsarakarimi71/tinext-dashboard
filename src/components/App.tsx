import 'react-toastify/dist/ReactToastify.min.css'
import { ToastContainer } from 'react-toastify'
import { CookiesProvider } from 'react-cookie'
import {
  QueryCache,
  QueryClient,
  QueryClientProvider
} from '@tanstack/react-query'
import AppRouter from '@/routes'
import { displayToast } from '@/lib'

const queryClient = new QueryClient({
  queryCache: new QueryCache({
    onError: (err) => {
      // @ts-ignore
      displayToast(err.message, 'error')
    }
  }),
  defaultOptions: {
    queries: {
      retry: 0,
      refetchOnWindowFocus: false
    }
  }
})

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <CookiesProvider>
        <div>
          <AppRouter />
          <ToastContainer rtl />
        </div>
      </CookiesProvider>
    </QueryClientProvider>
  )
}

export default App

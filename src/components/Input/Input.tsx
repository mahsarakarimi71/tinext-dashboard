import * as React from 'react'
import { GeneralProps } from '@/types'

type InputProps = React.HTMLProps<HTMLInputElement> & {
  model?: 'normal' | 'disable' | 'error'
  ltr?: boolean
  classes?: string
} & GeneralProps

type LabelProps = React.HTMLProps<HTMLLabelElement> & {
  model?: 'normal' | 'disable'
  text: string
} & GeneralProps

const normalInputClass =
  'bg-white border border-gray-100 text-masterBlack text-sm block w-full p-4 rounded-full'

const errorInputClass =
  'bg-white border border-tRed text-masterBlack text-sm block w-full p-4 rounded-full'
const disableInputClass =
  'bg-tGray border border-gray-100 text-gray-200 text-sm block w-full p-4 rounded-full cursor-not-allowed'
const normalLabelClass = 'block mb-1 mr-7 text-sm font-medium text-masterBlack'
const disableLabelClass = 'block mb-1 mr-7 text-sm font-medium text-gray-200'

const getInputClass = (model: InputProps['model']) => {
  switch (model) {
    case 'normal':
      return normalInputClass
    case 'disable':
      return disableInputClass
    case 'error':
      return errorInputClass
    default:
      return normalInputClass
  }
}

const getLabelClass = (model: LabelProps['model']) => {
  switch (model) {
    case 'normal':
      return normalLabelClass
    case 'disable':
      return disableLabelClass
    default:
      return normalLabelClass
  }
}

const Input = ({ children }: GeneralProps) => <>{children}</>

type Ref = HTMLInputElement

// eslint-disable-next-line react/display-name
const InputEditor = React.forwardRef<Ref, InputProps>(
  ({ model = 'normal', ltr, classes, ...restProps }, ref) => {
    const inputClass = getInputClass(model)
    const ltrStyle = ltr ? 'text-left' : ''
    return (
      <input
        ref={ref}
        type={'text'}
        className={`${inputClass} ${ltrStyle} ${
          classes || ''
        } outline-none placeholder:text-right focus:border-primary-400`}
        style={{ direction: ltr ? 'ltr' : 'rtl' }}
        disabled={model === 'disable'}
        {...restProps}
      />
    )
  }
)

const InputLabel = ({ model = 'normal', text, ...restProps }: LabelProps) => {
  const labelClass = getLabelClass(model)
  return (
    <label className={`mb-5 ${labelClass}`} {...restProps}>
      {text}
    </label>
  )
}

Input.Editor = InputEditor
Input.Label = InputLabel

export default Input

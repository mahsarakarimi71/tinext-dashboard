import Heading from '../Heading/Heading'

type Props = {
  activeStep: number
}

type BulletProps = {
  title: string
  bulletNumber: string
  isActive: boolean
}

const STEPS = [
  {
    no: 1,
    title: 'اطلاعات گیرنده و مرسوله'
  },
  {
    no: 2,
    title: 'مبدا و نوع جمع آوری'
  },
  {
    no: 3,
    title: 'زمان بندی'
  },
  {
    no: 4,
    title: 'پرداخت'
  }
]

const NewOrderStepProgress = ({ activeStep }: Props) => {
  return (
    <div className="mb-36 flex w-full flex-col items-center justify-between md:flex-row">
      <Heading title="ثبت سفارش جدید" />
      <div className="relative mt-20 flex items-center justify-center md:mt-0">
        <div className="w-[300px] border-b-2 border-dashed md:w-[400px] lg:w-[530px]" />
        <div className="absolute inset-x-0 -top-6 flex w-full justify-between lg:items-center">
          {STEPS.map((step) => {
            return (
              <Bullet
                key={step.no}
                title={step.title}
                bulletNumber={step.no.toString()}
                isActive={activeStep >= step.no}
              />
            )
          })}
        </div>
      </div>
      <div />
    </div>
  )
}

const Bullet = ({ title, bulletNumber, isActive }: BulletProps) => {
  const isLast = bulletNumber === STEPS.length.toString()
  const isFirst = bulletNumber === '1'
  const classes = isLast
    ? 'items-end'
    : isFirst
    ? 'items-start'
    : 'items-center -right-5'
  const titlePosition = isFirst ? 'lg:-right-12' : isLast ? '-left-3' : ''
  const bgColor = isActive ? 'bg-primary-500' : 'bg-tGray'
  const txtColor = isActive ? 'text-primary-500' : 'text-masterBlack'
  return (
    <div className={`relative flex ${!isLast && 'flex-1'} flex-col ${classes}`}>
      <div
        className={`flex h-10 w-10 items-center justify-center rounded-t-full ${bgColor}`}
      >
        <h4
          className={`text-3xl ${isActive ? 'text-white' : 'text-masterBlack'}`}
        >
          {bulletNumber}
        </h4>
      </div>
      <h3
        className={`relative ${titlePosition} mt-3 ${txtColor} max-w-[80px] lg:max-w-none`}
      >
        {title}
      </h3>
    </div>
  )
}

export default NewOrderStepProgress

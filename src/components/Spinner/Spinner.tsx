const sizes = {
  small: 'h-6 w-6 border-4 border-t-4',
  medium: 'h-16 w-16 border-16 border-t-16',
  large: 'h-32 w-32 border-32 border-t-32'
}

type Props = {
  size?: keyof typeof sizes
}

const Spinner = ({ size = 'small' }: Props) => (
  <div
    className={`${sizes[size]} animate-spin rounded-full border-gray-100 border-t-white`}
  />
)

export default Spinner
